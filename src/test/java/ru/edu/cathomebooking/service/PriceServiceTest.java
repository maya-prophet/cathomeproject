package ru.edu.cathomebooking.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import ru.edu.cathomebooking.dto.BookingDTO;
import ru.edu.cathomebooking.dto.PriceDTO;
import ru.edu.cathomebooking.mapper.PriceMapper;
import ru.edu.cathomebooking.model.Price;
import ru.edu.cathomebooking.repository.PriceRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.edu.cathomebooking.PriceTestData.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PriceServiceTest extends GenericServiceTest<Price, PriceDTO> {

    private final PriceRepository priceRepository = Mockito.mock(PriceRepository.class);
    private final PriceMapper priceMapper = Mockito.mock(PriceMapper.class);

    protected PriceServiceTest() {
        super();
        service = new PriceService(priceRepository, priceMapper);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(priceRepository.findAll()).thenReturn(PRICE_LIST);
        Mockito.when(priceMapper.toDTOs(PRICE_LIST)).thenReturn(PRICE_DTO_LIST);
        List<PriceDTO> priceDTOs = service.getAll();
        assertEquals(PRICE_LIST.size(), priceDTOs.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(priceRepository.findById(1L)).thenReturn(Optional.of(PRICE_1));
        Mockito.when(priceMapper.toDTO(PRICE_1)).thenReturn(PRICE_DTO_1);
        PriceDTO priceDTO = service.getOne(1L);
        log.info("Testing getOne(): {}", priceDTO);
        assertEquals(PRICE_DTO_1, priceDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(priceMapper.toEntity(PRICE_DTO_2)).thenReturn(PRICE_2);
        Mockito.when(priceMapper.toDTO(PRICE_2)).thenReturn(PRICE_DTO_2);
        Mockito.when(priceRepository.save(PRICE_2)).thenReturn(PRICE_2);
        PriceDTO priceDTO = service.create(PRICE_DTO_2);
        log.info("Testing create(): {}", priceDTO);
        assertEquals(PRICE_DTO_2, priceDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(priceMapper.toEntity(PRICE_DTO_2)).thenReturn(PRICE_2);
        Mockito.when(priceMapper.toDTO(PRICE_2)).thenReturn(PRICE_DTO_2);
        Mockito.when(priceRepository.save(PRICE_2)).thenReturn(PRICE_2);
        log.info("Testing update() - price before update: {}", PRICE_DTO_2.getPrice());
        PRICE_DTO_2.setPrice(BigDecimal.valueOf(700));
        PriceDTO priceDTO = service.update(PRICE_DTO_2);
        log.info("Testing update() - price after update: {}", priceDTO.getPrice());
        assertEquals(PRICE_DTO_2, priceDTO);
    }

    @Test
    @Order(5)
    protected void getStayPrices() {
        Mockito.when(priceMapper.toDTOs(PRICE_LIST_FOR_STAY_1)).thenReturn(PRICE_DTO_LIST_FOR_STAY_1);
        Mockito.when(priceRepository.getAllByStayId(1L)).thenReturn(PRICE_LIST_FOR_STAY_1);
        List<PriceDTO> priceDTOs = ((PriceService) service).getStayPrices(1L);
        assertEquals(PRICE_DTO_LIST_FOR_STAY_1.size(), priceDTOs.size());
    }

    @Test
    @Order(6)
    protected void checkIfValidPeriod() {
        Mockito.when(priceRepository.validatePeriod(PRICE_DTO_1.getFromDate(), PRICE_DTO_1.getToDate(), 1L))
                .thenReturn(true);
        assertTrue(((PriceService) service).checkIfValidPeriod(PRICE_DTO_1, 1L));
    }

    @Test
    @Order(7)
    protected void updateDefaultPrice() {
        PRICE_DTO_4.setStayId(null);
        Mockito.when(priceMapper.toDTOs(PRICE_LIST_FOR_STAY_1)).thenReturn(PRICE_DTO_LIST_FOR_STAY_1);
        Mockito.when(priceRepository.getAllByStayId(1L)).thenReturn(PRICE_LIST_FOR_STAY_1);
        Mockito.when(priceMapper.toEntity(PRICE_DTO_1)).thenReturn(PRICE_1);
        Mockito.when(priceMapper.toDTO(PRICE_1)).thenReturn(PRICE_DTO_1);
        Mockito.when(priceRepository.save(PRICE_1)).thenReturn(PRICE_1);
        Mockito.when(priceMapper.toEntity(PRICE_DTO_2)).thenReturn(PRICE_2);
        Mockito.when(priceMapper.toDTO(PRICE_2)).thenReturn(PRICE_DTO_2);
        Mockito.when(priceRepository.save(PRICE_2)).thenReturn(PRICE_2);
        log.info("Testing updateDefaultPrice() - price before update: {}", PRICE_DTO_2.getPrice());
        ((PriceService) service).updateDefaultPrice(PRICE_DTO_4, 1L);
        log.info("Testing updateDefaultPrice() - price after update: {}", PRICE_DTO_2.getPrice());
        assertEquals(PRICE_DTO_4.getPrice(), PRICE_DTO_2.getPrice());
    }
}
