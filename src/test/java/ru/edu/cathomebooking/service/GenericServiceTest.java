package ru.edu.cathomebooking.service;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.edu.cathomebooking.constants.UserRoleConstants;
import ru.edu.cathomebooking.dto.GenericDTO;
import ru.edu.cathomebooking.exception.DeleteException;
import ru.edu.cathomebooking.model.GenericModel;
import ru.edu.cathomebooking.service.userdetails.CustomUserDetails;

import java.util.List;

public abstract class GenericServiceTest<E extends GenericModel, D extends GenericDTO> {
    protected GenericService<E, D> service;

    @BeforeEach
    void init() {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                CustomUserDetails
                        .builder()
                        .username("user")
                        .id(1)
                        .authorities(List.of(new SimpleGrantedAuthority("ROLE_" + UserRoleConstants.STAY_OWNER)))
                        .build(),
                null,
                null);
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    }

    protected abstract void getAll();
    protected abstract void getOne();
    protected abstract void create();
    protected abstract void update();
}
