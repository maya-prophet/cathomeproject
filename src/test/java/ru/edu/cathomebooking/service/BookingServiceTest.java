package ru.edu.cathomebooking.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import ru.edu.cathomebooking.dto.BookingDTO;
import ru.edu.cathomebooking.dto.ProfileBookingInfoDTO;
import ru.edu.cathomebooking.exception.CancelBookingException;
import ru.edu.cathomebooking.mapper.BookingMapper;
import ru.edu.cathomebooking.mapper.UserMapper;
import ru.edu.cathomebooking.model.Booking;
import ru.edu.cathomebooking.model.Role;
import ru.edu.cathomebooking.repository.BookingRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.edu.cathomebooking.BookingTestData.*;
import static ru.edu.cathomebooking.StayTestData.STAY_DTO_1;
import static ru.edu.cathomebooking.UserTestData.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BookingServiceTest extends GenericServiceTest<Booking, BookingDTO> {

    private final BookingRepository bookingRepository = Mockito.mock(BookingRepository.class);
    private final BookingMapper bookingMapper = Mockito.mock(BookingMapper.class);

    private final UserService userService = Mockito.mock(UserService.class);
    private final UserMapper userMapper = Mockito.mock(UserMapper.class);
    private final StayService stayService = Mockito.mock(StayService.class);

    private final MailService mailService = Mockito.mock(MailService.class);

    protected BookingServiceTest() {
        super();
        service = new BookingService(bookingRepository, bookingMapper, userService, userMapper, stayService, mailService);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(bookingRepository.findAll()).thenReturn(BOOKING_LIST);
        Mockito.when(bookingMapper.toDTOs(BOOKING_LIST)).thenReturn(BOOKING_DTO_LIST);
        List<BookingDTO> bookingDTOs = service.getAll();
        assertEquals(BOOKING_LIST.size(), bookingDTOs.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(bookingRepository.findById(1L)).thenReturn(Optional.of(BOOKING_1));
        Mockito.when(bookingMapper.toDTO(BOOKING_1)).thenReturn(BOOKING_DTO_1);
        BookingDTO bookingDTO = service.getOne(1L);
        log.info("Testing getOne(): {}", bookingDTO);
        assertEquals(BOOKING_DTO_1, bookingDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(bookingMapper.toEntity(BOOKING_DTO_1)).thenReturn(BOOKING_1);
        Mockito.when(bookingMapper.toDTO(BOOKING_1)).thenReturn(BOOKING_DTO_1);
        Mockito.when(bookingRepository.save(BOOKING_1)).thenReturn(BOOKING_1);
        Mockito.when(userService.getOne(BOOKING_DTO_1.getGuestId())).thenReturn(GUEST_DTO);
        Mockito.when(stayService.getOne(BOOKING_DTO_1.getStayId())).thenReturn(STAY_DTO_1);
        Mockito.when(userService.getOne(STAY_DTO_1.getOwnerId())).thenReturn(OWNER_DTO);
        Mockito.when(mailService.sendSuccessfulBookingEmail(BOOKING_DTO_1, GUEST_DTO, OWNER_DTO, STAY_DTO_1))
                .thenReturn(true);
        BookingDTO bookingDTO = service.create(BOOKING_DTO_1);
        log.info("Testing create(): {}", bookingDTO);
        assertEquals(BOOKING_DTO_1, bookingDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(bookingMapper.toEntity(BOOKING_DTO_1)).thenReturn(BOOKING_1);
        Mockito.when(bookingMapper.toDTO(BOOKING_1)).thenReturn(BOOKING_DTO_1);
        Mockito.when(bookingRepository.save(BOOKING_1)).thenReturn(BOOKING_1);
        BOOKING_DTO_1.setCanceled(true);
        BookingDTO bookingDTO = service.update(BOOKING_DTO_1);
        log.info("Testing update() - canceled booking: {}", bookingDTO.isCanceled());
        assertTrue(bookingDTO.isCanceled());
    }

    @Test
    @Order(5)
    protected void getBookingsInfo() {
        OWNER.setRole(new Role(2L, null, null));
        PageRequest pageRequest = PageRequest.of(0, 10);
        Mockito.when(userService.getOne(1L)).thenReturn(OWNER_DTO);
        Mockito.when(userMapper.toEntity(OWNER_DTO)).thenReturn(OWNER);
        Mockito.when(bookingRepository.getOwnerBookingsInfo(OWNER, pageRequest))
                .thenReturn(new PageImpl<>(PROFILE_BOOKING_INFO_DTO_LIST));
        Page<ProfileBookingInfoDTO> bookings = ((BookingService) service).getBookingsInfo(1L, pageRequest);
        log.info("Testing getBookingsInfo(): {}", bookings);
        assertEquals(PROFILE_BOOKING_INFO_DTO_LIST, bookings.getContent());
    }

    @Test
    @Order(6)
    protected void cancel() {
        Mockito.when(bookingRepository.findById(2L)).thenReturn(Optional.of(BOOKING_2));
        Mockito.when(bookingMapper.toEntity(BOOKING_DTO_2)).thenReturn(BOOKING_2);
        Mockito.when(bookingMapper.toDTO(BOOKING_2)).thenReturn(BOOKING_DTO_2);
        Mockito.when(bookingRepository.save(BOOKING_2)).thenReturn(BOOKING_2);
        Mockito.when(stayService.getOne(BOOKING_DTO_2.getStayId())).thenReturn(STAY_DTO_1);
        try {
            ((BookingService) service).cancel(2L);
        } catch (CancelBookingException ex) {
            log.info("Testing cancel(), caught CancelBookingException: {}", ex.getMessage());
        }
        log.info("Testing cancel() booking canceled: {}", BOOKING_DTO_2.isCanceled());
        assertTrue(BOOKING_DTO_2.isCanceled());
    }
}
