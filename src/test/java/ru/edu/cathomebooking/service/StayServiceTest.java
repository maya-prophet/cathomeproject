package ru.edu.cathomebooking.service;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import ru.edu.cathomebooking.dto.OwnerStayDTO;
import ru.edu.cathomebooking.dto.StayCityDTO;
import ru.edu.cathomebooking.dto.StayDTO;
import ru.edu.cathomebooking.exception.DeleteException;
import ru.edu.cathomebooking.mapper.StayMapper;
import ru.edu.cathomebooking.model.Stay;
import ru.edu.cathomebooking.repository.StayRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static ru.edu.cathomebooking.StayTestData.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class StayServiceTest extends GenericServiceTest<Stay, StayDTO> {

    private final StayRepository stayRepository = Mockito.mock(StayRepository.class);
    private final StayMapper stayMapper = Mockito.mock(StayMapper.class);

    public StayServiceTest() {
        super();
        service = new StayService(stayRepository, stayMapper);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(stayRepository.findAll()).thenReturn(STAY_LIST);
        Mockito.when(stayMapper.toDTOs(STAY_LIST)).thenReturn(STAY_DTO_LIST);
        List<StayDTO> stayDTOs = service.getAll();
        assertEquals(STAY_LIST.size(), stayDTOs.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(stayRepository.findById(1L)).thenReturn(Optional.of(STAY_1));
        Mockito.when(stayMapper.toDTO(STAY_1)).thenReturn(STAY_DTO_1);
        StayDTO stayDTO = service.getOne(1L);
        log.info("Testing getOne(): {}", stayDTO);
        assertEquals(STAY_DTO_1, stayDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {
       Mockito.when(stayMapper.toEntity(STAY_DTO_1)).thenReturn(STAY_1);
       Mockito.when(stayMapper.toDTO(STAY_1)).thenReturn(STAY_DTO_1);
       Mockito.when(stayRepository.save(STAY_1)).thenReturn(STAY_1);
       StayDTO stayDTO = service.create(STAY_DTO_1);
        log.info("Testing create(): {}", stayDTO);
       assertEquals(STAY_DTO_1, stayDTO);

    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(stayMapper.toEntity(STAY_DTO_1)).thenReturn(STAY_1);
        Mockito.when(stayMapper.toDTO(STAY_1)).thenReturn(STAY_DTO_1);
        Mockito.when(stayRepository.save(STAY_1)).thenReturn(STAY_1);
        StayDTO stayDTO = service.update(STAY_DTO_1);
        log.info("Testing update(): {}", stayDTO);
        assertEquals(STAY_DTO_1, stayDTO);
    }

    @Test
    @Order(5)
    protected void delete() {
        Mockito.when(stayRepository.checkStayForDeletion(STAY_1)).thenReturn(false);
        Mockito.when(stayRepository.save(STAY_1)).thenReturn(STAY_1);
        Mockito.when(stayRepository.findById(1L)).thenReturn(Optional.of(STAY_1));
        try {
            ((StayService)service).deleteSoft(1L);
        } catch (DeleteException ex) {
            log.info("Testing delete(), cannot be deleted: {}", ex.getMessage());
        }
        assertFalse(STAY_1.isDeleted());
    }

    @Test
    @Order(6)
    protected void restore() {
        Mockito.when(stayRepository.save(STAY_2)).thenReturn(STAY_2);
        Mockito.when(stayRepository.findById(2L)).thenReturn(Optional.of(STAY_2));
        log.info("Before restore: {}", STAY_2);
        ((StayService) service).restore(2L);
        log.info("After restore: {}", STAY_2);
        assertFalse(STAY_2.isDeleted());
    }

    @Test
    @Order(7)
    protected void getOwnerStays() {
        PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "name"));
        Mockito.when(stayRepository.getNotDeletedStaysWithOccupiedSpotAmount(1L, pageRequest))
                .thenReturn(new PageImpl<>(OWNER_STAY_DTO_LIST));
        Page<OwnerStayDTO> ownerStayDTOs = ((StayService) service).getOwnerStays(1L, pageRequest);
        log.info("Testing getOwnerStays(): {}", ownerStayDTOs.getContent());
        assertEquals(OWNER_STAY_DTO_LIST, ownerStayDTOs.getContent());
    }

    @Test
    @Order(8)
    protected void getAllCities() {
        Mockito.when(stayRepository.getAllCities()).thenReturn(STAY_CITY_DTO_LIST);
        List<StayCityDTO> stayCityDTOs = ((StayService) service).getAllCities();
        assertEquals(STAY_CITY_DTO_LIST, stayCityDTOs);
    }

    @Test
    @Order(9)
    protected void getStayByName() {
        Mockito.when(stayRepository.getStayByName("stay1")).thenReturn(STAY_1);
        Mockito.when(stayMapper.toDTO(STAY_1)).thenReturn(STAY_DTO_1);
        StayDTO stayDTO = ((StayService) service).getStayByName("stay1");
        log.info("Testing getStayByName(): {}", stayDTO);
        assertEquals(STAY_DTO_1, stayDTO);
    }

}
