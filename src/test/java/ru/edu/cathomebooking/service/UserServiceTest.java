package ru.edu.cathomebooking.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.mapper.UserMapper;
import ru.edu.cathomebooking.model.User;
import ru.edu.cathomebooking.repository.UserRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static ru.edu.cathomebooking.UserTestData.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserServiceTest extends GenericServiceTest<User, UserDTO> {

    private final UserRepository userRepository = Mockito.mock(UserRepository.class);
    private final UserMapper userMapper = Mockito.mock(UserMapper.class);

    private final MailService mailService = Mockito.mock(MailService.class);
    private final BCryptPasswordEncoder bCryptPasswordEncoder = Mockito.mock(BCryptPasswordEncoder.class);


    public UserServiceTest() {
        super();
        service = new UserService(userMapper, userRepository, mailService, bCryptPasswordEncoder);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(userRepository.findAll()).thenReturn(USER_LIST);
        Mockito.when(userMapper.toDTOs(USER_LIST)).thenReturn(USER_DTO_LIST);
        List<UserDTO> userDTOs = service.getAll();
        assertEquals(USER_LIST.size(), userDTOs.size());

    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(userRepository.findById(1L)).thenReturn(Optional.of(OWNER));
        Mockito.when(userMapper.toDTO(OWNER)).thenReturn(OWNER_DTO);
        UserDTO userDTO = service.getOne(1L);
        log.info("Testing getOne(): {}", userDTO);
        assertEquals(OWNER_DTO, userDTO);

    }

    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(bCryptPasswordEncoder.encode("password1"))
                .thenReturn("$2a$10$/0Jzqi4Kg5.y4zhRrAmMeOZhHGKvEO3/qQ5ZaZkXLa58zE86Y2rYO");
        Mockito.when(userMapper.toEntity(OWNER_DTO)).thenReturn(OWNER);
        Mockito.when(userMapper.toDTO(OWNER)).thenReturn(OWNER_DTO);
        Mockito.when(userRepository.save(OWNER)).thenReturn(OWNER);
        UserDTO userDTO = service.create(OWNER_DTO);
        log.info("Testing create(): {}", userDTO);
        assertEquals(OWNER_DTO, userDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(userMapper.toEntity(OWNER_DTO)).thenReturn(OWNER);
        Mockito.when(userMapper.toDTO(OWNER)).thenReturn(OWNER_DTO);
        Mockito.when(userRepository.save(OWNER)).thenReturn(OWNER);
        UserDTO userDTO = service.update(OWNER_DTO);
        log.info("Testing update(): {}", userDTO);
        assertEquals(OWNER_DTO, userDTO);
    }

    @Test
    @Order(5)
    protected void getUserByLogin() {
        Mockito.when(userRepository.findUserByLogin("login1")).thenReturn(OWNER);
        Mockito.when(userMapper.toDTO(OWNER)).thenReturn(OWNER_DTO);
        UserDTO userDTO = ((UserService)service).getUserByLogin("login1");
        log.info("Testing getUserByLogin(): {}", userDTO);
        assertEquals("login1", userDTO.getLogin());
    }

    @Test
    @Order(6)
    protected void getUserByEmail() {
        Mockito.when(userRepository.findUserByEmail("email1")).thenReturn(OWNER);
        Mockito.when(userMapper.toDTO(OWNER)).thenReturn(OWNER_DTO);
        UserDTO userDTO = ((UserService)service).getUserByEmail("email1");
        log.info("Testing getUserByEmail(): {}", userDTO);
        assertEquals("email1@example.com", userDTO.getEmail());
    }

    @Test
    @Order(7)
    protected void getGuestEmailsWithUpcomingBookings() {
        Mockito.when(userRepository.getEmailsForUpcomingBookings(LocalDate.now().minusDays(1))).
                thenReturn(List.of(GUEST.getEmail()));
        List<String> emails = ((UserService) service).getGuestEmailsWithUpcomingBookings(LocalDate.now().minusDays(1));
        assertEquals(1, emails.size());
    }

    @Test
    @Order(8)
    protected void changePassword() {
        GUEST.setChangePasswordToken("8e2327ab-44d9-43ae-9f14-4c12cc8aaef6");
        Mockito.when(userRepository.findByChangePasswordToken("8e2327ab-44d9-43ae-9f14-4c12cc8aaef6"))
                .thenReturn(GUEST);
        Mockito.when(userMapper.toDTO(GUEST)).thenReturn(GUEST_DTO);
        Mockito.when(bCryptPasswordEncoder.encode("newpassword"))
                .thenReturn("$2a$10$VXayXYR3WvyDRJEsMqDNL.V.zS2/eU1JLZMVp6jUYVBo.CnIvISbi");
        ((UserService) service).changePassword("8e2327ab-44d9-43ae-9f14-4c12cc8aaef6", "newpassword");
        log.info("Testing changePassword(), {}", GUEST_DTO);
        assertNull(GUEST_DTO.getChangePasswordToken());
    }
}
