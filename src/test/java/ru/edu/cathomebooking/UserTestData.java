package ru.edu.cathomebooking;

import ru.edu.cathomebooking.dto.RoleDTO;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.model.Role;
import ru.edu.cathomebooking.model.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public interface UserTestData {

    UserDTO OWNER_DTO = new UserDTO("login1",
            "password1",
            "email1@example.com",
            "firstName1",
            "lastName1",
            "middleName1",
            "1980-01-01",
            "8-888-888-88-88",
            "city1",
            new RoleDTO(),
            null,
            null,
            new ArrayList<>(),
            2L);

    UserDTO GUEST_DTO = new UserDTO("login2",
            "password2",
            "email2@example.com",
            "firstName2",
            "lastName2",
            "middleName2",
            "1990-01-01",
            "8-888-888-88-88",
            "city2",
            new RoleDTO(1L, "Роль котовладельца", "description2"),
            null,
            new ArrayList<>(),
            null,
            1L);

    List<UserDTO> USER_DTO_LIST = List.of(OWNER_DTO, GUEST_DTO);

    User OWNER = new User("login1",
            "password1",
            "email1@example.com",
            "firstName1",
            "lastName1",
            "middleName1",
            LocalDate.parse("1980-01-01"),
            "8-888-888-88-88",
            "city1",
            new Role(),
            null,
            null,
            new ArrayList<>());

    User GUEST = new User("login2",
            "password2",
            "email2@example.com",
            "firstName2",
            "lastName2",
            "middleName2",
            LocalDate.parse("1990-01-01"),
            "8-888-888-88-88",
            "city2",
            new Role(1L, "Роль котовладельца", "description2"),
            null,
            new ArrayList<>(),
            null);

    List<User> USER_LIST = List.of(OWNER, GUEST);

    UserDTO INTEGRATION_GUEST_DTO = new UserDTO("maya",
            "$2a$10$PIhKE9jmCmX7ZuCHHRm6vOPXb2pRHPv3Dyi9KV9OFZT9t700DPBvC",
            "f-chit@yandex.ru",
            "Мая",
            "Апрель",
            "Февралёвна",
            "1999-05-18",
            "+7(984) 288-22-61",
            "Москва",
            new RoleDTO(1L),
            null,
            null,
            null,
            null);
}
