package ru.edu.cathomebooking;

import ru.edu.cathomebooking.dto.OwnerStayDTO;
import ru.edu.cathomebooking.dto.StayCityDTO;
import ru.edu.cathomebooking.dto.StayDTO;
import ru.edu.cathomebooking.dto.StayOfferDTO;
import ru.edu.cathomebooking.model.Stay;
import ru.edu.cathomebooking.model.StayType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static ru.edu.cathomebooking.UserTestData.OWNER;
import static ru.edu.cathomebooking.UserTestData.OWNER_DTO;

public interface StayTestData {

    StayDTO STAY_DTO_1 = new StayDTO("stay1",
            "city1",
            "address1",
            1.0, 1.0, 2,
            null,
            StayType.IN_HOME_BOARDING,
            "description1",
            true, true, true, true, true, true,
            new ArrayList<>(),
            new ArrayList<>(),
            24L);

    StayDTO STAY_DTO_1_UPD = new StayDTO("stay1",
            "city123",
            "address123",
            1.0, 1.0, 2,
            null,
            StayType.IN_HOME_BOARDING,
            "description123",
            true, true, true, true, true, true,
            new ArrayList<>(),
            new ArrayList<>(),
            24L);

    StayDTO STAY_DTO_2 = new StayDTO("stay2",
            "city2",
            "address2",
            1.0,1.0, 1,
            null,
            StayType.IN_HOME_BOARDING,
            "description2",
            true, true, true, true, true, true,
            new ArrayList<>(),
            new ArrayList<>(),
            24L);

    List<StayDTO> STAY_DTO_LIST = List.of(STAY_DTO_1, STAY_DTO_2);

    Stay STAY_1 = new Stay("stay1",
            "city1",
            "address1",
            1.0, 1.0, 2,
            OWNER,
            StayType.HOTEL,
            "description1",
            true, true, true, true, true, true,
            new ArrayList<>(),
            new ArrayList<>(),
            new ArrayList<>(),
            false,
            null,
            null);

    Stay STAY_2 = new Stay("stay2",
            "city2",
            "address2",
            1.0,1.0, 1,
            OWNER,
            StayType.HOTEL,
            "description2",
            true, true, true, true, true, true,
            new ArrayList<>(),
            new ArrayList<>(),
            new ArrayList<>(),
            true,
            null,
            null);

    List<Stay> STAY_LIST = List.of(STAY_1, STAY_2);

    OwnerStayDTO OWNER_STAY_DTO = new OwnerStayDTO(1L, "name1", "city1", "address1", 2, 1L);

    List<OwnerStayDTO> OWNER_STAY_DTO_LIST = List.of(OWNER_STAY_DTO);

    StayCityDTO STAY_CITY_DTO = new StayCityDTO("city1");
    List<StayCityDTO> STAY_CITY_DTO_LIST = List.of(STAY_CITY_DTO);

    StayOfferDTO STAY_OFFER = new StayOfferDTO() {
        @Override
        public Long getId() {
            return 51L;
        }

        @Override
        public String getName() {
            return "stay1";
        }

        @Override
        public String getCity() {
            return "city1";
        }

        @Override
        public String getAddress() {
            return "address1";
        }

        @Override
        public Double getLongitude() {
            return null;
        }

        @Override
        public Double getLatitude() {
            return null;
        }

        @Override
        public Integer getSpotAmount() {
            return 5;
        }

        @Override
        public Long getOwner() {
            return 24L;
        }

        @Override
        public StayType getType() {
            return StayType.IN_HOME_BOARDING;
        }

        @Override
        public String getDescription() {
            return null;
        }

        @Override
        public Boolean getHasToys() {
            return null;
        }

        @Override
        public Boolean getHasScratchingPost() {
            return null;
        }

        @Override
        public Boolean getHasIndTray() {
            return null;
        }

        @Override
        public Boolean getHasVideo() {
            return null;
        }

        @Override
        public Boolean getHasFood() {
            return null;
        }

        @Override
        public Boolean getIndividualStay() {
            return null;
        }

        @Override
        public String getPhotoUrls() {
            return null;
        }

        @Override
        public BigDecimal getTotalCost() {
            return BigDecimal.valueOf(10000);
        }
    };

    List<StayOfferDTO> STAY_OFFERS = List.of(STAY_OFFER);
}
