package ru.edu.cathomebooking;

import ru.edu.cathomebooking.dto.BookingDTO;
import ru.edu.cathomebooking.dto.ProfileBookingInfoDTO;
import ru.edu.cathomebooking.model.Booking;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static ru.edu.cathomebooking.StayTestData.STAY_1;
import static ru.edu.cathomebooking.StayTestData.STAY_2;
import static ru.edu.cathomebooking.UserTestData.GUEST;

public interface BookingTestData {

    BookingDTO BOOKING_DTO_1 = new BookingDTO(LocalDate.now(),
            LocalDate.now().plusDays(1),
            1,
            BigDecimal.valueOf(10000.0),
            false,
            1L,
            1L);

    BookingDTO BOOKING_DTO_2 = new BookingDTO(LocalDate.now().plusDays(3),
            LocalDate.now().plusDays(1),
            1,
            BigDecimal.valueOf(10000.0),
            false,
            2L,
            1L);

    List<BookingDTO> BOOKING_DTO_LIST = List.of(BOOKING_DTO_1, BOOKING_DTO_2);

    Booking BOOKING_1 = new Booking(STAY_1,
            GUEST,
            LocalDate.now(),
            LocalDate.now().plusDays(7),
            1,
            BigDecimal.valueOf(10000.0),
            false);

    Booking BOOKING_2 = new Booking(STAY_2,
            GUEST,
            LocalDate.now(),
            LocalDate.now().plusDays(5),
            2,
            BigDecimal.valueOf(20000.0),
            false);

    List<Booking> BOOKING_LIST = List.of(BOOKING_1, BOOKING_2);

    ProfileBookingInfoDTO PROFILE_BOOKING_INFO_DTO_1 = new ProfileBookingInfoDTO(1L,
            LocalDate.now(),
            LocalDate.now().plusDays(7),
            1,
            BigDecimal.valueOf(10000.0),
            1L,
            "stay1",
            "city1",
            "address1",
            false);

    ProfileBookingInfoDTO PROFILE_BOOKING_INFO_DTO_2 = new ProfileBookingInfoDTO(2L,
            LocalDate.now(),
            LocalDate.now().plusDays(5),
            2,
            BigDecimal.valueOf(10000.0),
            2L,
            "stay2",
            "city2",
            "address2",
            false);

    List<ProfileBookingInfoDTO> PROFILE_BOOKING_INFO_DTO_LIST = List.of(PROFILE_BOOKING_INFO_DTO_1, PROFILE_BOOKING_INFO_DTO_2);
}
