package ru.edu.cathomebooking;

import ru.edu.cathomebooking.dto.PriceDTO;
import ru.edu.cathomebooking.model.Price;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static ru.edu.cathomebooking.StayTestData.STAY_1;
import static ru.edu.cathomebooking.StayTestData.STAY_2;


public interface PriceTestData {

    PriceDTO PRICE_DTO_1 = new PriceDTO(34L,
            LocalDate.parse("2023-10-10"),
            LocalDate.parse("2023-10-13"),
            BigDecimal.valueOf(500));

    PriceDTO PRICE_DTO_2 = new PriceDTO(34L,
            null,
            null,
            BigDecimal.valueOf(700));

    PriceDTO PRICE_DTO_3 = new PriceDTO(2L,
            LocalDate.parse("2023-04-15"),
            LocalDate.parse("2023-04-17"),
            BigDecimal.valueOf(600));

    PriceDTO PRICE_DTO_4 = new PriceDTO(2L,
            null,
            null,
            BigDecimal.valueOf(700));

    List<PriceDTO> PRICE_DTO_LIST = List.of(PRICE_DTO_1, PRICE_DTO_2, PRICE_DTO_3, PRICE_DTO_4);
    List<PriceDTO> PRICE_DTO_LIST_FOR_STAY_1 = List.of(PRICE_DTO_1, PRICE_DTO_2);

    Price PRICE_1 = new Price(STAY_1,
            LocalDate.parse("2023-04-10"),
            LocalDate.parse("2023-04-13"),
            BigDecimal.valueOf(500));

    Price PRICE_2 = new Price(STAY_1,
            null,
            null,
            BigDecimal.valueOf(600));

    Price PRICE_3 = new Price(STAY_2,
            LocalDate.parse("2023-04-15"),
            LocalDate.parse("2023-04-17"),
            BigDecimal.valueOf(600));

    Price PRICE_4 = new Price(STAY_2,
            null,
            null,
            BigDecimal.valueOf(700));

    List<Price> PRICE_LIST = List.of(PRICE_1, PRICE_2, PRICE_3, PRICE_4);
    List<Price> PRICE_LIST_FOR_STAY_1 = List.of(PRICE_1, PRICE_2);
}
