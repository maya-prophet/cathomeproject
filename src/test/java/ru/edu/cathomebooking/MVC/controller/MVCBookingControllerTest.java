package ru.edu.cathomebooking.MVC.controller;

import com.icegreen.greenmail.configuration.GreenMailConfiguration;
import com.icegreen.greenmail.junit5.GreenMailExtension;
import com.icegreen.greenmail.util.ServerSetupTest;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import ru.edu.cathomebooking.dto.BookingDTO;
import ru.edu.cathomebooking.repository.BookingRepository;
import ru.edu.cathomebooking.service.BookingService;
import ru.edu.cathomebooking.service.MailService;


import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.edu.cathomebooking.BookingTestData.BOOKING_DTO_1;
import static ru.edu.cathomebooking.StayTestData.STAY_DTO_1;
import static ru.edu.cathomebooking.UserTestData.GUEST_DTO;
import static ru.edu.cathomebooking.UserTestData.OWNER_DTO;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Slf4j
public class MVCBookingControllerTest extends CommonTestMVC {

//    @MockBean
//    private MailService mailService;

    @RegisterExtension
    static GreenMailExtension greenMail = new GreenMailExtension(ServerSetupTest.SMTP)
            .withConfiguration(GreenMailConfiguration.aConfig().withUser("user", "password"))
            .withPerMethodLifecycle(false);

    @Autowired
    private BookingService bookingService;

    @Test
    @Order(0)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void viewBookings() throws Exception {
        log.info("Тест по выведению формы просмотра бронирований начат успешно");
        mvc.perform(get("/bookings"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению формы просмотра бронирований завершен");
    }

    @Test
    @Order(1)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void bookingRequesting() throws Exception {
        log.info("Тест по бронированию начат успешно");
        mvc.perform(post("/bookings/bookingRequest")
                                .flashAttr("bookingRequestForm", BOOKING_DTO_1)
                    )
                .andDo(print())
                .andExpect(flash().attributeExists("bookingRequest"))
                .andExpect(flash().attributeExists("stayInfo"))
                .andExpect(flash().attributeExists("guestInfo"))
                .andExpect(redirectedUrl("/bookings/book"));
        log.info("Тест по бронированию завершен");
    }

    @Test
    @Order(2)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void bookForm() throws Exception {
        log.info("Тест по выведению формы просмотра инфо по бронированию начат успешно");
        mvc.perform(get("/bookings/book")
                        .flashAttr("bookingRequest", BOOKING_DTO_1)
                        .flashAttr("stayInfo", STAY_DTO_1)
                        .flashAttr("guestInfo", GUEST_DTO)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению формы просмотра инфо по бронированию завершен");
    }

    @Test
    @Order(3)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void book() throws Exception {
        log.info("Тест по созданию бронирования начат успешно");
//        Mockito.when(mailService.sendSuccessfulBookingEmail(BOOKING_DTO_1, GUEST_DTO, OWNER_DTO, STAY_DTO_1))
//                .thenReturn(true);
        mvc.perform(post("/bookings/book")
                        .flashAttr("bookingForm", BOOKING_DTO_1))
                .andDo(print())
                .andExpect(redirectedUrl("/bookings/success"));
        Awaitility.await().atMost(3, TimeUnit.SECONDS).untilAsserted(()-> {
            assertEquals(2, greenMail.getReceivedMessages().length);
            assertTrue(greenMail.getReceivedMessages()[0].getContent().toString().contains("Ваше брониронвание"));
            assertTrue(greenMail.getReceivedMessages()[1].getContent().toString().contains("Было получено новое бронирование"));
        });
        log.info("Тест по созданию бронирования завершен");
    }

    @Test
    @Order(4)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void bookingSuccess() throws Exception {
        log.info("Тест по выведению страницы успеха бронирования успешно");
        mvc.perform(get("/bookings/success"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению страницы успеха бронирования завершен");
    }

    @Test
    @Order(5)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void cancel() throws Exception {
        log.info("Тест по отмене бронирования успешно");
        BookingDTO bookingToCancel = bookingService.getOne(32L);
        bookingToCancel.setCanceled(false);
        bookingService.update(bookingToCancel);
        mvc.perform(get("/bookings/cancel/32"))
                .andDo(print())
                .andExpect(redirectedUrl("/bookings"));
        assertTrue(bookingService.getOne(32L).isCanceled());
        log.info("Тест по отмене бронирования завершен");
    }
}
