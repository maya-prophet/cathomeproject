package ru.edu.cathomebooking.MVC.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import ru.edu.cathomebooking.dto.PriceDTO;
import ru.edu.cathomebooking.service.PriceService;
import ru.edu.cathomebooking.service.StayService;
import ru.edu.cathomebooking.service.userdetails.CustomUserDetails;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.edu.cathomebooking.PriceTestData.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Slf4j
public class MVCPriceControllerTest extends CommonTestMVC {

    //менять на актуальный при тесте на удаление
    int deletePriceId = 73;

    @Autowired
    private StayService stayService;
    @Autowired
    private PriceService priceService;
    private int ownerId;
    private Long stayId = 34L;



    @Test
    @Order(0)
    @WithUserDetails(value = "alex", userDetailsServiceBeanName="customUserDetailsService")
    protected void viewStayPrices() throws Exception {
        log.info("Тест по просмотру цен места размещения начат успешно");
        ownerId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
        mvc.perform(get("/prices/" + ownerId + "/" + stayId)
                        .flashAttr("exception", "exception occurred"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по просмотру цен места размещения завершен");
    }

    @Test
    @Order(1)
    @WithUserDetails(value = "alex", userDetailsServiceBeanName="customUserDetailsService")
    protected void updatePrice() throws Exception {
        log.info("Тест по добавлению нового ценового периода начат успешно");
        ownerId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
        mvc.perform(post("/prices/update/" + ownerId + "/" + stayId)
                        .flashAttr("newPrice", PRICE_DTO_1)
                )
                .andDo(print())
                .andExpect(redirectedUrl("/prices/" + ownerId + "/" + stayId));
        log.info("Тест по добавлению нового ценового периода завершен");
    }

    @Test
    @Order(2)
    @WithUserDetails(value = "alex", userDetailsServiceBeanName="customUserDetailsService")
    protected void updateDefaultPrice() throws Exception {
        log.info("Тест по обновлению стандартной цены начат успешно");
        ownerId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
        mvc.perform(post("/prices/update/default/" + ownerId + "/" + stayId)
                        .flashAttr("defaultPrice", PRICE_DTO_2)
                )
                .andDo(print())
                .andExpect(redirectedUrl("/prices/" + ownerId + "/" + stayId));
        log.info("Тест по обновлению стандартной цены завершен");
    }

    @Test
    @Order(3)
    @WithUserDetails(value = "alex", userDetailsServiceBeanName="customUserDetailsService")
    protected void delete() throws Exception {
        log.info("Тест по просмотру цен места размещения начат успешно");
        ownerId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
        mvc.perform(get("/prices/delete/" + ownerId + "/" + stayId + "/" + deletePriceId))
                .andDo(print())
                .andExpect(redirectedUrl("/prices/" + ownerId + "/" + stayId));
        log.info("Тест по просмотру цен места размещения завершен");
    }
}
