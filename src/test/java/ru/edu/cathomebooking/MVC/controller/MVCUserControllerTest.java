package ru.edu.cathomebooking.MVC.controller;

import com.icegreen.greenmail.configuration.GreenMailConfiguration;
import com.icegreen.greenmail.junit5.GreenMailExtension;
import com.icegreen.greenmail.util.ServerSetupTest;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.model.User;
import ru.edu.cathomebooking.repository.UserRepository;



import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.edu.cathomebooking.UserTestData.GUEST_DTO;
import static ru.edu.cathomebooking.UserTestData.INTEGRATION_GUEST_DTO;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Slf4j
public class MVCUserControllerTest extends CommonTestMVC {

    @Autowired
    private UserRepository userRepository;

    private static UUID uuid;

    @RegisterExtension
    static GreenMailExtension greenMail = new GreenMailExtension(ServerSetupTest.SMTP)
            .withConfiguration(GreenMailConfiguration.aConfig().withUser("user", "password"))
            .withPerMethodLifecycle(false);

    @Test
    @Order(0)
    protected void registrationForm() throws Exception {
        log.info("Тест по выведению формы регистрации начат успешно");
        mvc.perform(get("/users/registration"))
                .andDo(print())
                .andExpect(model().attribute("userForm", new UserDTO()))
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению формы регистрации завершен");
    }

    @Test
    @Order(1)
    protected void registration() throws Exception {
        log.info("Тест по регистрации начат успешно");
        mvc.perform(post("/users/registration")
                        .flashAttr("userForm", GUEST_DTO))
                .andDo(print())
                .andExpect(redirectedUrl("/login"));
        log.info("Тест по регистрации завершен");
        userRepository.delete(userRepository.findUserByLogin(GUEST_DTO.getLogin()));
    }

    @Test
    @Order(2)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void viewGuestProfile() throws Exception {
        log.info("Тест по выведению профиля начат успешно");
        mvc.perform(get("/users/profile"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению профиля завершен");
    }

    @Test
    @Order(3)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void updateGuestInfo() throws Exception {
        log.info("Тест по регистрации начат успешно");
        mvc.perform(post("/users/update-user-info")
                        .flashAttr("userProfile", INTEGRATION_GUEST_DTO))
                .andDo(print())
                .andExpect(redirectedUrl("/users/profile"));
        log.info("Тест по регистрации завершен");
    }

    @Test
    @Order(4)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void rememberPasswordForm() throws Exception {
        log.info("Тест по выведению формы смены пароля начат успешно");
        mvc.perform(get("/users/remember-password"))
                .andDo(print())
                .andExpect(model().attribute("changePasswordForm", new UserDTO()))
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению формы смены пароля завершен");
    }

    @Test
    @Order(5)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void rememberPassword() throws Exception {
        log.info("Тест вспомнить пароль начат успешно");
        log.info("GreenMail is running: {}", greenMail.isRunning());
        mvc.perform(post("/users/remember-password")
                        .flashAttr("changePasswordForm", INTEGRATION_GUEST_DTO))
                .andDo(print())
                .andExpect(redirectedUrl("/login"));
        Awaitility.await().atMost(3, TimeUnit.SECONDS).untilAsserted(()-> {
                    assertEquals(1, greenMail.getReceivedMessages().length);
                    assertTrue(greenMail.getReceivedMessages()[0].getContent().toString().contains("Для восстановления пароля перейдите по ссылке"));
                });
        String message = greenMail.getReceivedMessages()[0].getContent().toString();
        uuid = UUID.fromString(message.substring(message.indexOf("=") + 1));
        log.info("UUID: {}", uuid);
        log.info("Тест вспомнить пароль завершен");
    }

    @Test
    @Order(6)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void changePasswordForm() throws Exception {
        log.info("Тест по выведению формы установки нового пароля начат успешно");
        mvc.perform(get("/users/change-password"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению формы установки нового пароля завершен");
    }

    @Test
    @Order(7)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void changePassword() throws Exception {
        log.info("Тест по смене пароля начат успешно");
        mvc.perform(post("/users/change-password")
                        .param("uuid", uuid.toString())
                        .flashAttr("changePasswordForm", INTEGRATION_GUEST_DTO))
                .andDo(print())
                .andExpect(redirectedUrl("/login"));
        log.info("Тест по смене пароля завершен");
    }

    @Test
    @Order(8)
    @WithUserDetails(value = "maya", userDetailsServiceBeanName="customUserDetailsService")
    protected void changePasswordInProfile() throws Exception {
        log.info("Тест по смене пароля в профиле начат успешно");
        mvc.perform(get("/users/change-password/user"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        User user = userRepository.findUserByLogin("maya");
        user.setChangePasswordToken(null);
        userRepository.save(user);
        log.info("Тест по смене пароля в профиле завершен");
    }
}
