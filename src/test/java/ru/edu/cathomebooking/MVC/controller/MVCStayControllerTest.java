package ru.edu.cathomebooking.MVC.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.RequestBuilder;
import ru.edu.cathomebooking.dto.AvailableStaysRequestDTO;
import ru.edu.cathomebooking.model.Stay;
import ru.edu.cathomebooking.repository.StayRepository;
import ru.edu.cathomebooking.service.StayService;
import ru.edu.cathomebooking.service.userdetails.CustomUserDetails;

import java.time.LocalDate;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static ru.edu.cathomebooking.StayTestData.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Slf4j
public class MVCStayControllerTest extends CommonTestMVC {
    @Autowired
    private StayRepository stayRepository;
    @Autowired
    private StayService stayService;

    @Test
    @Order(0)
    protected void findAvailableStays() throws Exception {
        log.info("Тест по поиску доступных мест размещения по параметрам начат успешно");
        AvailableStaysRequestDTO requestDTO = new AvailableStaysRequestDTO("Москва",
                                                                                2,
                                                                                LocalDate.now(),
                                                                                LocalDate.now().plusDays(5));
        RequestBuilder request = post("/stays/findAvailableStays", requestDTO);
        mvc.perform(request)
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/stays/availableStays"));
        log.info("Тест по поиску доступных мест размещения по параметрам завершен");
    }

    @Test
    @Order(1)
    @WithUserDetails(value = "alex", userDetailsServiceBeanName="customUserDetailsService")
    protected void addStayForm() throws Exception {
        log.info("Тест по выведению формы добавления нового места размещения начат успешно");
        mvc.perform(get("/stays/add"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению формы добавления нового места размещения завершен");
    }

    @Test
    @Order(2)
    @WithUserDetails(value = "alex", userDetailsServiceBeanName="customUserDetailsService")
    protected void addStay() throws Exception {
        log.info("Тест по добавлению нового места размещения начат успешно");
        RequestBuilder request = post("/stays/add")
                .flashAttr("addStayForm", STAY_DTO_1);
        mvc.perform(request)
                .andDo(print())
                .andExpect(redirectedUrl("/"));
        log.info("Тест по добавлению нового места размещения завершен");
    }

    @Test
    @Order(3)
    protected void viewStay() throws Exception {
        log.info("Тест по выведению формы места размещения начат успешно");
        Long stayId = stayRepository.getStayByName(STAY_DTO_1.getName()).getId();
        mvc.perform(get("/stays/" + stayId))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению формы места размещения завершен");
    }

    @Test
    @Order(4)
    @WithUserDetails(value = "alex", userDetailsServiceBeanName="customUserDetailsService")
    protected void getOwnerStays() throws Exception {
        log.info("Тест по выведению формы всех мест размещения арендодателя начат успешно");
        mvc.perform(get("/stays/owner-stays"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению формы всех мест размещения арендодателя завершен");
    }

    @Test
    @Order(5)
    @WithUserDetails(value = "alex", userDetailsServiceBeanName="customUserDetailsService")
    protected void updateStayForm() throws Exception {
        log.info("Тест по выведению формы изменения места размещения начат успешно");
        int ownerId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
        Long stayId = stayRepository.getStayByName(STAY_DTO_1.getName()).getId();
        mvc.perform(get("/stays/update/" + ownerId + "/" + stayId)
                        .flashAttr("updateStayForm", STAY_DTO_1))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по выведению формы изменения места размещения завершен");
    }

    @Test
    @Order(6)
    @WithUserDetails(value = "alex", userDetailsServiceBeanName="customUserDetailsService")
    protected void updateStay() throws Exception {
        log.info("Тест по изменению места размещения начат успешно");
        int ownerId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
        Long stayId = stayRepository.getStayByName(STAY_DTO_1.getName()).getId();
        RequestBuilder request = post("/stays/update/" + ownerId + "/" + stayId)
                .flashAttr("updateStayForm", STAY_DTO_1_UPD);
        mvc.perform(request)
                .andDo(print())
                .andExpect(redirectedUrl("/stays/owner-stays"));
        Stay foundUpdatedStay = stayRepository.getStayByName(STAY_DTO_1.getName());
        assertEquals(STAY_DTO_1_UPD.getAddress(), foundUpdatedStay.getAddress());
        log.info("Тест по изменению места размещения завершен");
    }


    @Test
    @Order(7)
    @WithUserDetails(value = "alex", userDetailsServiceBeanName="customUserDetailsService")
    protected void deleteStay() throws Exception {
        log.info("Тест по удалению места размещения арендодателя начат успешно");
        int ownerId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
        Long stayId = stayRepository.getStayByName(STAY_DTO_1.getName()).getId();
        mvc.perform(get("/stays/delete/" + ownerId + "/" + stayId))
                .andDo(print())
                .andExpect(redirectedUrl("/stays/owner-stays"));
        Stay foundUpdatedStay = stayRepository.getStayByName(STAY_DTO_1.getName());
        assertTrue(foundUpdatedStay.isDeleted());
        log.info("Тест по удалению места размещения арендодателя завершен");
        stayService.deleteHard(stayId);
    }


}
