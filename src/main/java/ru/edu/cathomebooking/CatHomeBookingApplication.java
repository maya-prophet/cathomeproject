package ru.edu.cathomebooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CatHomeBookingApplication {

    public static void main(String[] args) {
        SpringApplication.run(CatHomeBookingApplication.class, args);
    }

}
