package ru.edu.cathomebooking.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.cathomebooking.model.Price;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PriceRepository extends GenericRepository<Price> {

    Page<Price> getAllByStayId(Long stayId, Pageable pageable);

    @Query("""
        SELECT p FROM Price p
        WHERE p.stay.id = :stayId
        AND (YEAR(p.toDate) = YEAR(CURRENT_DATE)
        OR (p.fromDate IS NULL AND p.toDate IS NULL))
        ORDER BY p.fromDate, p.toDate
            """)
    List<Price> getAllByStayId(@Param("stayId") Long stayId);


    @Query("""
        SELECT CASE WHEN count(p) > 0 THEN FALSE ELSE TRUE END
        FROM Price p JOIN p.stay
        WHERE p.stay.id = :stayId
        AND (:fromDate BETWEEN p.fromDate AND p.toDate
                OR :toDate BETWEEN p.fromDate AND p.toDate
                OR (:fromDate < p.fromDate AND :toDate > p.toDate))       
            """)
    boolean validatePeriod(@Param(value = "fromDate") LocalDate fromDate,
                           @Param(value = "toDate") LocalDate toDate,
                           @Param(value = "stayId") Long stayId);
}
