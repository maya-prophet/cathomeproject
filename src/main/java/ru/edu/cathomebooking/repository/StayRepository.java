package ru.edu.cathomebooking.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.cathomebooking.dto.OwnerStayDTO;
import ru.edu.cathomebooking.dto.StayCityDTO;
import ru.edu.cathomebooking.dto.StayOfferDTO;
import ru.edu.cathomebooking.model.Stay;
import ru.edu.cathomebooking.constants.QueryConstants;


import java.time.LocalDate;
import java.util.List;

@Repository
public interface StayRepository extends GenericRepository<Stay> {

    @Query(nativeQuery = true, countQuery = QueryConstants.AVAILABLE_STAYS_WITH_AVAILABLE_SPOTS_COUNT,
            value = QueryConstants.AVAILABLE_STAYS_WITH_AVAILABLE_SPOTS)
    Page<StayOfferDTO> getAvailableStaysForParams(@Param("city") String city,
                                                  @Param("catAmount") Integer catAmount,
                                                  @Param("dateBegin") LocalDate dateBegin,
                                                  @Param("dateEnd") LocalDate dateEnd,
                                                  Pageable pageable);

    @Query ("SELECT distinct new ru.edu.cathomebooking.dto.StayCityDTO(s.city) FROM Stay s")
    List<StayCityDTO> getAllCities();


    Stay getStayByName(String name);

    Page<Stay> getStaysByOwnerIdAndIsDeletedFalse(Long ownerId, Pageable pageable);

    @Query("""
                SELECT CASE WHEN count(b) > 0 THEN FALSE ELSE TRUE END
                FROM Stay s JOIN s.bookings b
                WHERE s = :stay
                AND b.dateEnd >= current_date 
                AND b.isCanceled = false
            """)
    boolean checkStayForDeletion(@Param("stay") Stay stay);



    @Query("""
              SELECT new ru.edu.cathomebooking.dto.OwnerStayDTO(s.id,
                                                                s.name,
                                                                s.city,
                                                                s.address,
                                                                s.spotAmount,
                                                                SUM(COALESCE(b.catAmount, 0)))
              FROM Stay s LEFT JOIN s.bookings b
              ON current_date BETWEEN b.dateBegin AND b.dateEnd
              WHERE s.isDeleted = false
              AND s.owner.id = :ownerId
              GROUP BY s
           """)
    Page<OwnerStayDTO> getNotDeletedStaysWithOccupiedSpotAmount(@Param("ownerId") Long ownerId, Pageable pageable);
}
