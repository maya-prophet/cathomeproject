package ru.edu.cathomebooking.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.model.User;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface UserRepository extends GenericRepository<User> {

    User findUserByLogin(String login);

    User findUserByEmail(String email);

    @Query("""
        SELECT u.email FROM User u JOIN u.bookings b
        WHERE b.dateBegin = :date 
        """)
    List<String> getEmailsForUpcomingBookings(@Param(value = "date")LocalDate referenceDate);

    User findByChangePasswordToken(String uuid);
}
