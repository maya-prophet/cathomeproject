package ru.edu.cathomebooking.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.cathomebooking.constants.QueryConstants;
import ru.edu.cathomebooking.dto.ProfileBookingInfoDTO;
import ru.edu.cathomebooking.model.Booking;
import ru.edu.cathomebooking.model.User;

import java.util.List;

@Repository
public interface BookingRepository extends GenericRepository<Booking> {

    @Query(value = QueryConstants.GUEST_PROFILE_BOOKINGS_INFO)
    Page<ProfileBookingInfoDTO> getGuestBookingsInfo(@Param(value = "guest")User guest,
                                                     Pageable pageable);

    @Query(value = QueryConstants.OWNER_PROFILE_BOOKINGS_INFO)
    Page<ProfileBookingInfoDTO> getOwnerBookingsInfo(@Param(value = "owner")User owner,
                                                     Pageable pageable);
}
