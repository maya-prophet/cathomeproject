package ru.edu.cathomebooking.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.edu.cathomebooking.dto.AvailableStaysRequestDTO;
import ru.edu.cathomebooking.dto.StayDTO;
import ru.edu.cathomebooking.dto.StayOfferDTO;
import ru.edu.cathomebooking.model.Stay;
import ru.edu.cathomebooking.repository.StayRepository;
import ru.edu.cathomebooking.service.StayService;

import java.util.List;

@RestController
@RequestMapping("/rest/stays")
@Tag(name = "Места размещения",
        description = "Контроллер для работы с местами размещения")
public class StayController extends GenericController<Stay, StayDTO>{

    private final StayService stayService;
    private final StayRepository stayRepository;

    public StayController(StayService stayService,
                          StayRepository stayRepository) {
        super(stayService);
        this.stayService = stayService;
        this.stayRepository = stayRepository;
    }


}
