package ru.edu.cathomebooking.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
//import ru.edu.cathomebooking.config.jwt.JWTUtil;
import ru.edu.cathomebooking.dto.BookingDTO;
import ru.edu.cathomebooking.dto.LoginDTO;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.model.User;
import ru.edu.cathomebooking.service.UserService;
import ru.edu.cathomebooking.service.userdetails.CustomUserDetailsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/rest/users")
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователями сервиса бронирования")
public class UserController extends GenericController<User, UserDTO> {

    private final UserService userService;
    private final CustomUserDetailsService customUserDetailsService;
//    private final JWTUtil jwtUtil;

    public UserController(UserService userService,
                          CustomUserDetailsService customUserDetailsService){
//                          JWTUtil jwtUtil) {
        super(userService);
        this.userService = userService;
        this.customUserDetailsService = customUserDetailsService;
//        this.jwtUtil = jwtUtil;
    }


//    @PostMapping("/auth")
//    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
//        Map<String, Object> response = new HashMap<>();
//        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
//        if (!userService.checkPassword(loginDTO.getPassword(), foundUser)) {
//            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка аутентификации. Неверный пароль");
//        }
//        String token = jwtUtil.generateToken(foundUser);
//        response.put("token", token);
//        response.put("authorities", foundUser.getAuthorities());
//        log.debug("TOKEN: {}", token);
//        return ResponseEntity.ok().body(response);
//    }
}
