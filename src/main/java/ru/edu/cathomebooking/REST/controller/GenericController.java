package ru.edu.cathomebooking.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.edu.cathomebooking.dto.GenericDTO;
import ru.edu.cathomebooking.exception.DeleteException;
import ru.edu.cathomebooking.model.GenericModel;
import ru.edu.cathomebooking.service.GenericService;

import java.util.List;

@RestController
@SecurityRequirement(name = "Bearer Authentication")
public abstract class GenericController<T extends GenericModel, N extends GenericDTO> {

    protected final GenericService<T, N> service;

    public GenericController(GenericService<T, N> service) {
        this.service = service;
    }

    //@RequestParam: localhost:8092/rest/users/getOneById?id=1
    @Operation(description = "Получить запись по ID", method = "getOneById")
    @GetMapping(value = "/getOneById", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> getOneById(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getOne(id));
    }

    @Operation(description = "Получить все записи", method = "getAll")
    @GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<N>> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(service.getAll());
    }

    @Operation(description = "Создать новую запись", method = "create")
    @PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE,
                                consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> create(@RequestBody N newEntity) {
//        newEntity.setCreatedWhen(LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(service.create(newEntity));
    }

    //находим по id то, что нам надо обновить, и в записи меняются поля
    //на поля из updatedEntity
    @Operation(description = "Обновить запись", method = "update")
    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE,
                                    consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> update(@RequestBody N updatedEntity,
                       @RequestParam(value = "id") Long id) {
        updatedEntity.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(service.update(updatedEntity));
    }

    //@PathVariable: localhost:9095/api/rest/users/delete/1
    @Operation(description = "Удалить запись по ID", method = "delete")
    @DeleteMapping(value = "/delete/{id}")
    public void delete(@PathVariable(value = "id") Long id) throws DeleteException {
        service.deleteHard(id);
    }


}
