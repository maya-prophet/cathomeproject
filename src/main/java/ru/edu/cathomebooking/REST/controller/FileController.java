package ru.edu.cathomebooking.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.edu.cathomebooking.service.FileService;
import org.springframework.http.HttpHeaders;



import java.util.List;

@RestController
@RequestMapping(value = "/rest/file")
@Tag(name = "Фотографии",
        description = "Контроллер для работы с фотографиями мест размещения")
@Slf4j

public class FileController {

    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    //entity: stay, user (whose photos you want)
    //entityId: id of the stay or user
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Получить все фотографии места размещения по его ID", method = "getFiles")
    public ResponseEntity<List<String>> getFiles(@RequestParam(value="entity") String entity,
                                                 @RequestParam(value = "entityId") Long id) {
        log.info("Id: " + id + "; entity: " + entity);
        return ResponseEntity.ok(fileService.getFileList(entity, id));
    }


    //работает
    @GetMapping(value="/{entity}/{entityId}/{filename}", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(description = "Получить фотографию", method = "getFile")
    public ResponseEntity<Resource> getFile(@PathVariable(value="entity") String entity,
                                            @PathVariable(value="entityId") Long id,
                                            @PathVariable(value="filename") String filename) {
        log.info("filename: " + filename);
        ByteArrayResource resource = new ByteArrayResource(fileService.getFile(entity, id, filename));
        return ResponseEntity.ok()
                .headers(this.headers(filename))
                .contentLength(resource.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }
}
