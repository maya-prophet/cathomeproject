package ru.edu.cathomebooking.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.cathomebooking.dto.BookingDTO;
import ru.edu.cathomebooking.model.Booking;
import ru.edu.cathomebooking.service.BookingService;

import java.util.List;

@RestController
@RequestMapping("/rest/bookings")
@Tag(name = "Бронирования",
        description = "Контроллер для работы с бронированиями")
public class BookingController extends GenericController<Booking, BookingDTO>{

    private final BookingService bookingService;

    public BookingController(BookingService bookingService) {
        super(bookingService);
        this.bookingService = bookingService;
    }

}
