package ru.edu.cathomebooking.dto;


import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends GenericDTO {
    private String login;

    private String password;

    private String email;

    private String firstName;

    private String lastName;

    private String middleName;

    private String birthDate;

    private String phone;

    private String city;

    private RoleDTO role;

    private String changePasswordToken;

    private List<Long> bookingIds;

    private List<Long> stayIds;

    private Long roleId;
}
