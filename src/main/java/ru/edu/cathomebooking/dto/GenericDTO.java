package ru.edu.cathomebooking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class GenericDTO {
    private Long id;

//    private LocalDateTime createdWhen;
//
//    private String createdBy;
}
