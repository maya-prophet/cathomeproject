package ru.edu.cathomebooking.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PriceDTO extends GenericDTO {
    private Long stayId;

    private LocalDate fromDate;

    private LocalDate toDate;

    private BigDecimal price;
}
