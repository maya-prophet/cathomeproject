package ru.edu.cathomebooking.dto;


//import com.blazebit.persistence.view.EntityView;
//import com.blazebit.persistence.view.MappingSingular;
import ru.edu.cathomebooking.model.StayType;

import java.math.BigDecimal;

//@EntityView(Stay.class)
public interface StayOfferDTO {

    Long getId();

    String getName();

    String getCity();

    String getAddress();

    Double getLongitude();

    Double getLatitude();

    Integer getSpotAmount();

    Long getOwner();

    StayType getType();

    String getDescription();

    Boolean getHasToys();

    Boolean getHasScratchingPost();

    Boolean getHasIndTray();

    Boolean getHasVideo();

    Boolean getHasFood();

    Boolean getIndividualStay();

//    @MappingSingular
//    List<String> getPhotoUrls();

    String getPhotoUrls();

    BigDecimal getTotalCost();

}
