package ru.edu.cathomebooking.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class OwnerStayDTO extends GenericDTO {
    private String name;
    private String city;
    private String address;
    private Integer spotAmount;
    private Long occupiedSpots;

    public OwnerStayDTO(Long id, String name, String city, String address, Integer spotAmount, Long occupiedSpots) {
        super(id);
        this.name = name;
        this.city = city;
        this.address = address;
        this.spotAmount = spotAmount;
        this.occupiedSpots = occupiedSpots;
    }
}
