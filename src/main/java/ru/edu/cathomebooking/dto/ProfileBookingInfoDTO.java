package ru.edu.cathomebooking.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
public class ProfileBookingInfoDTO {

    private Long bookingId;
    private LocalDate dateBegin;
    private LocalDate dateEnd;
    private Integer catAmount;
    private BigDecimal totalCost;

    //решила не таскать за собой кучу инфо из места размещения (фотки, флаги, цены)
    private Long stayId;
    private String name;
    private String city;
    private String address;
    private boolean isCanceled = false;
}
