package ru.edu.cathomebooking.dto;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AvailableStaysRequestDTO {
    private String city;
    private Integer catAmount;
    private LocalDate dateBegin;
    private LocalDate dateEnd;
}
