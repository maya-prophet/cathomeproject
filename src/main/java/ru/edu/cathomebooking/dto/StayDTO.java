package ru.edu.cathomebooking.dto;

import lombok.*;
import ru.edu.cathomebooking.model.StayType;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StayDTO extends GenericDTO {
    private String name;
    private String city;
    private String address;

    private Double longitude;

    private Double latitude;

    private Integer spotAmount;

    private UserDTO owner;

    private StayType type;

    private String description;

    private Boolean hasToys;

    private Boolean hasScratchingPost;

    private Boolean hasIndTray;

    private Boolean hasVideo;

    private Boolean hasFood;

    private Boolean isIndividualStay;

    private List<String> photoUrls = new ArrayList<>();

    private List<Long> bookingIds;

    private Long ownerId;

}
