package ru.edu.cathomebooking.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class StayCityDTO {
    String cityName;
}
