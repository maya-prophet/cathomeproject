package ru.edu.cathomebooking.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BookingDTO extends GenericDTO {

    private LocalDate dateBegin;

    private LocalDate dateEnd;

    private Integer catAmount;

    private BigDecimal totalCost;

    private boolean isCanceled = false;

    private Long stayId;
    private Long guestId;
}
