package ru.edu.cathomebooking.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ru.edu.cathomebooking.dto.*;
import ru.edu.cathomebooking.exception.DeleteException;
import ru.edu.cathomebooking.service.userdetails.CustomUserDetails;
import ru.edu.cathomebooking.repository.StayRepository;
import ru.edu.cathomebooking.service.FileService;
import ru.edu.cathomebooking.service.StayService;
import ru.edu.cathomebooking.config.cache.StayRequestSessionCache;
import ru.edu.cathomebooking.service.UserService;
import ru.edu.cathomebooking.utils.SecurityUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;


@Hidden
@Controller
@RequestMapping("/stays")
@Slf4j
public class MVCStayController {

    private final StayService stayService;
    private final UserService userService;
    private final FileService fileService;
    private StayRequestSessionCache requestSessionCache;

    public MVCStayController(StayService stayService,
                             StayRequestSessionCache requestSessionCache,
                             UserService userService,
                             FileService fileService) {
        this.stayService = stayService;
        this.requestSessionCache = requestSessionCache;
        this.userService = userService;
        this.fileService = fileService;
    }

    @PostMapping("/findAvailableStays")
    public String findAvailableStays(@ModelAttribute(value = "searchForm")AvailableStaysRequestDTO requestDTO) {
        requestSessionCache = new StayRequestSessionCache(requestDTO);
        return "redirect:/stays/availableStays";
    }

    @GetMapping("/availableStays")
    public String showAvailableStays(Model model,
                                     @RequestParam(value = "page", defaultValue = "1") int page,
                                     @RequestParam(value = "size", defaultValue = "4") int pageSize) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "name"));

        AvailableStaysRequestDTO requestDTO = new AvailableStaysRequestDTO();
        requestDTO.setCity(requestSessionCache.getCity());
        requestDTO.setDateBegin(requestSessionCache.getDateBegin());
        requestDTO.setDateEnd(requestSessionCache.getDateEnd());
        requestDTO.setCatAmount(requestSessionCache.getCatAmount());

        List<StayCityDTO> stayCities = stayService.getAllCities();
        model.addAttribute("stayCities", stayCities);

        if (requestDTO.getDateEnd().isBefore(requestDTO.getDateBegin().plusDays(1))) {
           model.addAttribute("availableStaysAttr", new PageImpl<>(Collections.emptyList()));
        } else {
            model.addAttribute("availableStaysAttr", stayService.getAvailableStays(requestDTO, pageRequest));
        }
        model.addAttribute("searchForm", requestDTO);
        return "stays/viewAvailableStays";
    }


    @GetMapping("/add")
    public String addStay(Model model) {
        model.addAttribute("addStayForm", new StayDTO());
        return "stays/addStayView";
    }

    @PostMapping("/add")
    public String addStay(@ModelAttribute("addStayForm")StayDTO stayDTO,
                          @ModelAttribute List<MultipartFile> files,
                          BindingResult bindingResult) {
        if (stayService.getStayByName(stayDTO.getName()) != null) {
            bindingResult.rejectValue("name", "error.name", "Такое имя уже существует");
            return "stays/addStayView";
        }
//        UserDTO ownerDTO = userService.getUserByEmail(stayDTO.getEmail());
//        if (ownerDTO == null || !ownerDTO.getRole().getTitle().equals("STAY_OWNER")) {
//            bindingResult.rejectValue("email", "error.email", "Пользователя с таким email не существует");
//            return "stays/addStayView";
//        }
//        ownerDTO = userService.getUserByEmail(stayDTO.getEmail());
//        log.info("Files: " + files.size());

        CustomUserDetails customUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.info("CustomUserDetails: {}", customUser);
        UserDTO ownerDTO = userService.getOne(Long.valueOf(customUser.getUserId()));
        StayDTO addedStayDTO = null;
        if (ownerDTO != null && ownerDTO.getRole().getId().equals(2L)) {
            stayDTO.setOwner(ownerDTO);
            addedStayDTO = stayService.create(stayDTO);
        }

        if(addedStayDTO != null && files != null) {
            for (MultipartFile file : files) {
                fileService.uploadFile(file, "stay", addedStayDTO.getId());
            }
        }

        return "redirect:/";
    }

    @GetMapping("/{id}")
    public String viewStay(@PathVariable("id") Long id,
                           Model model) {

        model.addAttribute("stay", stayService.getOne(id));
        return "stays/viewStay";
    }


    @GetMapping("/owner-stays")
    public String getOwnerStays(Model model,
                                @RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "5") int pageSize) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "name"));

        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Page<OwnerStayDTO> stays = stayService.getOwnerStays(Long.valueOf(customUserDetails.getUserId()), pageRequest);
        model.addAttribute("stays", stays);
        return "stays/ownerStaysView";
    }

    @GetMapping("/delete/{ownerId}/{stayId}")
    public String deleteStay(@PathVariable("ownerId") Long ownerId,
                             @PathVariable("stayId") Long stayId) throws DeleteException {
        if (SecurityUtils.checkUser(ownerId)) {
            stayService.deleteSoft(stayId);
        }
        return "redirect:/stays/owner-stays";
    }

    @GetMapping("/update/{ownerId}/{stayId}")
    public String updateStay(@PathVariable("ownerId") Long ownerId,
                             @PathVariable("stayId") Long stayId,
                             Model model) {
        if (SecurityUtils.checkUser(ownerId)) {
            model.addAttribute("updateStayForm", stayService.getOne(stayId));
        }
        log.info("Photo url: {}", stayService.getOne(stayId).getPhotoUrls().get(0));
        return "stays/updateStayInfo";
    }

    @PostMapping("/update/{ownerId}/{stayId}")
    public String updateStay(@PathVariable("ownerId") Long ownerId,
                             @PathVariable("stayId") Long stayId,
                             @ModelAttribute("updateStayForm") StayDTO stayDTO,
                             @ModelAttribute List<MultipartFile> files,
                             BindingResult bindingResult) {
        if (SecurityUtils.checkUser(ownerId)) {
            StayDTO stayNameDuplicated = stayService.getStayByName(stayDTO.getName());
            StayDTO stayFoundById = stayService.getOne(stayId);
            if (stayService.getStayByName(stayDTO.getName()) != null &&
                    !Objects.equals(stayNameDuplicated.getName(), stayFoundById.getName())) {
                bindingResult.rejectValue("name", "error.name", "Такое имя уже существует");
                return "stays/updateStayInfo";
            }
            log.info("Photo urls in DB size: {}", stayFoundById.getPhotoUrls());
            stayDTO.setId(stayId);
            stayDTO.setOwner(userService.getOne(ownerId));

            List<String> photoUrls = stayFoundById.getPhotoUrls();
            if (photoUrls != null && !photoUrls.isEmpty() && !photoUrls.get(0).isBlank())
                stayDTO.setPhotoUrls(photoUrls);

            stayService.update(stayDTO);
            if (files != null && !files.isEmpty()) {
                for (MultipartFile file : files) {
                    if (file.getOriginalFilename() != null && !file.getOriginalFilename().isBlank())
                        fileService.uploadFile(file, "stay", stayId);
                }
            }
        }
        return "redirect:/stays/owner-stays";
    }


    @ExceptionHandler(DeleteException.class)
    public RedirectView handleErrorAuth(HttpServletRequest req,
                                        Exception ex,
                                        RedirectAttributes redirectAttributes) {
        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());

        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/stays/owner-stays", true);
    }
}
