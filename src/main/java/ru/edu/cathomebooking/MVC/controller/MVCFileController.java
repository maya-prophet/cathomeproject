package ru.edu.cathomebooking.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.edu.cathomebooking.service.FileService;

import java.util.*;

@Hidden
@Controller
@RequestMapping("/file")
@Slf4j
public class MVCFileController {

    private final FileService fileService;

    public MVCFileController(FileService fileService) {
        this.fileService = fileService;
    }


    @GetMapping(value="/{entity}/{entityId}/{filename}", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable(value="entity") String entity,
                                            @PathVariable(value="entityId") Long id,
                                            @PathVariable(value="filename") String filename) {
        log.info("filename: " + filename);
        ByteArrayResource resource = new ByteArrayResource(fileService.getFile(entity, id, filename));
        return ResponseEntity.ok()
                .headers(this.headers(filename))
                .contentLength(resource.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }

    @PostMapping(value = "upload")
    public String uploadFiles (@RequestParam(value = "files")List<MultipartFile> files,
                               @RequestParam(value = "id") Long id) {
        if (files != null && !files.isEmpty()) {
            for (MultipartFile file : files) {
                if (Objects.nonNull(file)) {
                    fileService.uploadFile(file, "stay", id);
                }
            }
        }
        return "redirect:/";
    }

}
