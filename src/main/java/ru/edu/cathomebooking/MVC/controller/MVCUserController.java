package ru.edu.cathomebooking.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.websocket.server.PathParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.service.userdetails.CustomUserDetails;
import ru.edu.cathomebooking.service.BookingService;
import ru.edu.cathomebooking.service.StayService;
import ru.edu.cathomebooking.service.UserService;

import java.util.*;

import static ru.edu.cathomebooking.constants.UserRoleConstants.ADMIN;

@Hidden
@Controller
@RequestMapping("/users")
@Slf4j
public class MVCUserController {

    private final UserService userService;


    public MVCUserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserDTO());
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") UserDTO userDTO,
                               BindingResult bindingResult) {
        if (userDTO.getLogin().equalsIgnoreCase(ADMIN) || userService.getUserByLogin(userDTO.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "Такой логин уже существует");
            return "registration";
        }
        if (userService.getUserByEmail(userDTO.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email", "Такой email уже существует");
            return "registration";
        }
        userService.create(userDTO);
        return "redirect:/login";
    }

    @GetMapping("/profile")
    public String viewGuestProfile(Model model) {
        CustomUserDetails customUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = Long.valueOf(customUser.getUserId());
        UserDTO userDTO = userService.getOne(userId);
        model.addAttribute("userProfile", userDTO);
        return "users/viewProfile";
    }

    @PostMapping("/update-user-info")
    public String updateGuestInfo(@ModelAttribute(value="userProfile")UserDTO userDTOFromForm,
                                  BindingResult bindingResult) {
        CustomUserDetails customUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long securityId = Long.valueOf(customUser.getUserId());

        UserDTO userEmailDuplicated = userService.getUserByEmail(userDTOFromForm.getEmail());
        UserDTO foundUser = userService.getOne(securityId);
        if (userEmailDuplicated != null && !Objects.equals(userEmailDuplicated.getEmail(), foundUser.getEmail())) {
            bindingResult.rejectValue("email", "error.email", "Такой email уже существует");
            return "users/viewProfile";
        }
        foundUser.setFirstName(userDTOFromForm.getFirstName());
        foundUser.setLastName(userDTOFromForm.getLastName());
        foundUser.setMiddleName(userDTOFromForm.getMiddleName());
        foundUser.setCity(userDTOFromForm.getCity());
        foundUser.setPhone(userDTOFromForm.getPhone());
        foundUser.setBirthDate(userDTOFromForm.getBirthDate());
        foundUser.setEmail(userDTOFromForm.getEmail());
        userService.update(foundUser);
        return "redirect:/users/profile";
    }


    @GetMapping("/remember-password")
    public String rememberPassword(Model model) {
        model.addAttribute("changePasswordForm", new UserDTO());
        return "users/rememberPassword";
    }

    @PostMapping("/remember-password")
    public String rememberPassword(@ModelAttribute("changePasswordForm") UserDTO userDTO,
                                   BindingResult bindingResult) {
        userDTO = userService.getUserByEmail(userDTO.getEmail());
        if (userDTO == null) {
            bindingResult.rejectValue("email", "error.email", "Пользователя с таким email не существует");
            return "users/rememberPassword";
        }
        else {
            userService.sendChangePasswordEmail(userDTO);
            return "redirect:/login";
        }
    }

    @GetMapping("/change-password")
    public String changePassword(@PathParam(value = "uuid") String uuid,
                                 Model model) {
        model.addAttribute("uuid", uuid);
        return "users/changePassword";
    }

    @PostMapping("/change-password")
    public String changePassword(@PathParam(value = "uuid") String uuid,
                                 @ModelAttribute("changePasswordForm") UserDTO userDTO) {
        try {
            userService.changePassword(uuid, userDTO.getPassword());
        } catch (NotFoundException ex) {
            log.error("UserController#changePassword: {}", ex.getMessage());
            return "redirect:/error";
        }
        return "redirect:/login";
    }

    @GetMapping("/change-password/user")
    public String changePasswordInProfile(Model model) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDTO userDTO = userService.getOne(Long.valueOf(customUserDetails.getUserId()));
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        userService.update(userDTO);
        model.addAttribute("uuid", uuid);
        return "users/changePassword";
    }

}
