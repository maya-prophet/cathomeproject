package ru.edu.cathomebooking.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ru.edu.cathomebooking.constants.Errors;
import ru.edu.cathomebooking.dto.PriceDTO;
import ru.edu.cathomebooking.exception.DeleteException;
import ru.edu.cathomebooking.exception.PricePeriodOverlapException;
import ru.edu.cathomebooking.service.PriceService;
import ru.edu.cathomebooking.service.StayService;
import ru.edu.cathomebooking.utils.SecurityUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Hidden
@Controller
@RequestMapping("/prices")
@Slf4j
public class MVCPriceController {
    private final PriceService priceService;
    private final StayService stayService;

    public MVCPriceController(PriceService priceService,
                              StayService stayService) {
        this.priceService = priceService;
        this.stayService = stayService;
    }

    @GetMapping("/{ownerId}/{stayId}")
    public String viewStayPrices(Model model,
                                 @PathVariable("ownerId") Long ownerId,
                                 @PathVariable("stayId") Long stayId,
                                 @ModelAttribute(name = "exception") final String exception) {
        if (SecurityUtils.checkUser(ownerId)) {
            List<PriceDTO> prices = priceService.getStayPrices(stayId);
            log.info("Prices length: {}", prices.size());
            for(PriceDTO price : prices) {
                if (price.getFromDate() == null && price.getToDate() == null) {
                    model.addAttribute("defaultPrice", price);
                }
            }
            model.addAttribute("prices", prices);
            model.addAttribute("stay", stayService.getOne(stayId));
            model.addAttribute("newPrice", new PriceDTO());
            model.addAttribute("exception", exception);
            return "prices/viewStayPrices";
        } else {
            return "redirect:/";
        }

    }

    @PostMapping("/update/{ownerId}/{stayId}")
    public String updatePrice(@PathVariable("ownerId") Long ownerId,
                               @PathVariable("stayId") Long stayId,
                               @ModelAttribute("newPrice") PriceDTO price) throws PricePeriodOverlapException {
        if (SecurityUtils.checkUser(ownerId) &&
                price != null && price.getPrice() != null) {
            if (checkDates(price, stayId)) {
                log.info("Price: {}", price.getPrice());
                price.setStayId(stayId);
                priceService.create(price);
            } else {
                throw new PricePeriodOverlapException(Errors.Prices.PRICE_PERIOD_OVERLAP_ERROR);
            }
        }
        return "redirect:/prices/" + ownerId + "/" + stayId;
    }

    @PostMapping("/update/default/{ownerId}/{stayId}")
    public String updateDefaultPrice(@PathVariable("ownerId") Long ownerId,
                                     @PathVariable("stayId") Long stayId,
                                     @ModelAttribute("defaultPrice")PriceDTO defaultPrice) {
        if (SecurityUtils.checkUser(ownerId) &&
                defaultPrice != null && defaultPrice.getPrice() != null) {
            log.info("Default price stay id: {}", defaultPrice.getStayId());
            priceService.updateDefaultPrice(defaultPrice, stayId);
        }

        return "redirect:/prices/" + ownerId + "/" + stayId;
    }

    @GetMapping("/delete/{ownerId}/{stayId}/{priceId}")
    public String delete(@PathVariable("ownerId") Long ownerId,
                         @PathVariable("stayId") Long stayId,
                         @PathVariable("priceId") Long priceId) {
        if (SecurityUtils.checkUser(ownerId)) {
            priceService.deleteHard(priceId);
        }
        return "redirect:/prices/" + ownerId + "/" + stayId;
    }


    @ExceptionHandler(PricePeriodOverlapException.class)
    public RedirectView handleErrorAuth(HttpServletRequest req,
                                        Exception ex,
                                        RedirectAttributes redirectAttributes) {
        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());
        Map pathVariables = (Map) req.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        String ownerId = (String) pathVariables.get("ownerId");
        String stayId = (String) pathVariables.get("stayId");

        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/prices/"  + ownerId + "/" + stayId, true);
    }

    private boolean checkDates(PriceDTO price, Long stayId) {
        return price.getFromDate() != null &&
                price.getToDate() != null &&
                (price.getFromDate().isBefore(price.getToDate()) ||
                price.getFromDate().isEqual(price.getToDate())) &&
                price.getFromDate().isAfter(LocalDate.now().minusDays(1)) &&
                priceService.checkIfValidPeriod(price, stayId);
    }

}
