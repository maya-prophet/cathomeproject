package ru.edu.cathomebooking.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ru.edu.cathomebooking.dto.BookingDTO;
import ru.edu.cathomebooking.dto.ProfileBookingInfoDTO;
import ru.edu.cathomebooking.dto.StayDTO;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.exception.CancelBookingException;
import ru.edu.cathomebooking.service.userdetails.CustomUserDetails;
import ru.edu.cathomebooking.service.BookingService;
import ru.edu.cathomebooking.service.StayService;
import ru.edu.cathomebooking.service.UserService;

@Hidden
@Controller
@RequestMapping("/bookings")
@Slf4j
public class MVCBookingController {

    private final StayService stayService;
    private final UserService userService;
    private final BookingService bookingService;

    public MVCBookingController(StayService stayService,
                                UserService userService,
                                BookingService bookingService) {
        this.stayService = stayService;
        this.userService = userService;
        this.bookingService = bookingService;
    }

    @GetMapping
    public String viewBookings(Model model,
                               @RequestParam(value = "page", defaultValue = "1") int page,
                               @RequestParam(value = "size", defaultValue = "5") int pageSize){
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);

        CustomUserDetails customUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = Long.valueOf(customUser.getUserId());
        Page<ProfileBookingInfoDTO> bookings = bookingService.getBookingsInfo(userId, pageRequest);
        model.addAttribute("bookings", bookings);
        return "bookings/bookingsView";
    }

    @PostMapping("/bookingRequest")
    public String bookingRequesting(@ModelAttribute(name = "bookingRequestForm") BookingDTO bookingDTO,
                                 RedirectAttributes redirectAttributes) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long guestId = Long.valueOf(customUserDetails.getUserId());

        StayDTO stayDTO = stayService.getOne(bookingDTO.getStayId());
        UserDTO userDTO = userService.getOne(guestId);

        redirectAttributes.addFlashAttribute("bookingRequest", bookingDTO);
        redirectAttributes.addFlashAttribute("stayInfo", stayDTO);
        redirectAttributes.addFlashAttribute("guestInfo", userDTO);
        return "redirect:/bookings/book";

    }

    @GetMapping("/book")
    public String book() {
        return "/bookings/bookingFormView";
    }

    @PostMapping("/book")
    public String book(@ModelAttribute(value = "bookingForm") BookingDTO bookingDTO) {
        bookingService.create(bookingDTO);
        return "redirect:/bookings/success";
    }

    @GetMapping("/success")
    public String bookingSuccess() {
        return "/bookings/success";
    }

    @GetMapping("/cancel/{bookingId}")
    public String cancel(@PathVariable("bookingId") Long bookingId) throws CancelBookingException {
        bookingService.cancel(bookingId);
        return "redirect:/bookings";
    }

    @ExceptionHandler(CancelBookingException.class)
    public RedirectView handleErrorAuth(HttpServletRequest req,
                                        Exception ex,
                                        RedirectAttributes redirectAttributes) {
        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());

        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/bookings", true);
    }

}
