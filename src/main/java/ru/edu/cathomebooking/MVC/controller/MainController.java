package ru.edu.cathomebooking.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.cathomebooking.dto.StayCityDTO;
import ru.edu.cathomebooking.service.StayService;

import java.util.List;

@Hidden
@Controller
public class MainController {

    private final StayService stayService;

    public MainController(StayService stayService) {
        this.stayService = stayService;
    }

    @GetMapping("/")
    public String index(Model model) {
        List<StayCityDTO> stayCities = stayService.getAllCities();
        model.addAttribute("stayCities", stayCities);
        return "index";
    }
}
