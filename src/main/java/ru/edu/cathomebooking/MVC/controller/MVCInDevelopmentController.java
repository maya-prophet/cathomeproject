package ru.edu.cathomebooking.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Hidden
@Controller
public class MVCInDevelopmentController {

    @GetMapping("/in-development")
    public String inDevelopment() {
        return "inDevelopment";
    }
}
