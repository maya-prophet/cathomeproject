package ru.edu.cathomebooking.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.cathomebooking.constants.Errors;
import ru.edu.cathomebooking.dto.*;
import ru.edu.cathomebooking.exception.DeleteException;
import ru.edu.cathomebooking.mapper.StayMapper;
import ru.edu.cathomebooking.model.Stay;

import ru.edu.cathomebooking.repository.StayRepository;

import java.time.LocalDateTime;
import java.util.*;

@Service
@Slf4j
public class StayService extends GenericService<Stay, StayDTO> {

    private final StayRepository stayRepository;


    public StayService(StayRepository stayRepository,
                       StayMapper mapper) {
        super(stayRepository, mapper);
        this.stayRepository = stayRepository;
    }

    public List<StayCityDTO> getAllCities() {
        return stayRepository.getAllCities();
    }

    public Page<StayOfferDTO> getAvailableStays(AvailableStaysRequestDTO params, Pageable pageable) {
        Page<StayOfferDTO> stayOffersPaginated = stayRepository.getAvailableStaysForParams(params.getCity(),
                params.getCatAmount(), params.getDateBegin(), params.getDateEnd().minusDays(1), pageable);
        List<StayOfferDTO> result = stayOffersPaginated.getContent();
        return new PageImpl<>(result, pageable, stayOffersPaginated.getTotalElements());
    }

    public StayDTO getStayByName(String name) {
        return mapper.toDTO(stayRepository.getStayByName(name));
    }

    public Page<OwnerStayDTO> getOwnerStays(Long ownerId, Pageable pageable) {
//        Page<Stay> staysPaginated = stayRepository.getStaysByOwnerIdAndIsDeletedFalse(ownerId, pageable);
        Page<OwnerStayDTO> staysPaginated = stayRepository.getNotDeletedStaysWithOccupiedSpotAmount(ownerId, pageable);
        List<OwnerStayDTO> result = staysPaginated.getContent();

        return new PageImpl<>(result, pageable, staysPaginated.getTotalElements());
    }


    public void deleteSoft(Long id) throws DeleteException {
        Stay stay = stayRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Место размещения с заданным id=" + id + " не существует."));

        if (stayRepository.checkStayForDeletion(stay)) {
            stay.setDeleted(true);
            stay.setDeletedWhen(LocalDateTime.now());
            stay.setDeletedBy(SecurityContextHolder.getContext().getAuthentication().getName());
            stayRepository.save(stay);
        } else throw new DeleteException(Errors.Stays.STAY_DELETE_ERROR);
    }

    //админ сможет восстанавливать в админке, когда она появится
    public void restore(Long id) {
        Stay stay = stayRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Место размещения с заданным id=" + id + " не существует."));
        stay.setDeleted(false);
        stay.setDeletedWhen(null);
        stay.setDeletedBy(null);
        stayRepository.save(stay);
    }
}
