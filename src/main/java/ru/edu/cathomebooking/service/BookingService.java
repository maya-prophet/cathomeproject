package ru.edu.cathomebooking.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.cathomebooking.constants.Errors;
import ru.edu.cathomebooking.constants.MailConstants;
import ru.edu.cathomebooking.constants.UserRoleConstants;
import ru.edu.cathomebooking.dto.BookingDTO;
import ru.edu.cathomebooking.dto.ProfileBookingInfoDTO;
import ru.edu.cathomebooking.dto.StayDTO;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.exception.CancelBookingException;
import ru.edu.cathomebooking.mapper.BookingMapper;
import ru.edu.cathomebooking.mapper.UserMapper;
import ru.edu.cathomebooking.model.Booking;
import ru.edu.cathomebooking.model.User;
import ru.edu.cathomebooking.service.userdetails.CustomUserDetails;
import ru.edu.cathomebooking.repository.BookingRepository;
import ru.edu.cathomebooking.repository.UserRepository;
import ru.edu.cathomebooking.utils.MailUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ru.edu.cathomebooking.constants.FormatterConstant.FORMATTER_TO_USER;

@Service
public class BookingService extends GenericService<Booking, BookingDTO> {

    private final BookingRepository bookingRepository;
    private final UserService userService;
    private final UserMapper userMapper;
    private final StayService stayService;
    private final MailService mailService;

    public BookingService (BookingRepository bookingRepository,
                           BookingMapper mapper,
                           UserService userService,
                           UserMapper userMapper,
                           StayService stayService,
                           MailService mailService) {
        super(bookingRepository, mapper);
        this.bookingRepository = bookingRepository;
        this.userService = userService;
        this.userMapper = userMapper;
        this.stayService = stayService;
        this.mailService = mailService;
    }

//    public Page<ProfileBookingInfoDTO> getGuestBookingsInfo(Long guestId, Pageable pageable) {
//        User guest = userRepository.findById(guestId)
//                .orElseThrow(() -> new NotFoundException("Пользователь с таким ID не найден: " + guestId));
//        Page<ProfileBookingInfoDTO> profileBookingsPaginated = bookingRepository.getGuestBookingsInfo(guest, pageable);
//        List<ProfileBookingInfoDTO> result = profileBookingsPaginated.getContent();
//        return new PageImpl<>(result, pageable, profileBookingsPaginated.getTotalElements());
//    }
//
//    public Page<ProfileBookingInfoDTO> getOwnerBookingsInfo(Long ownerId, Pageable pageable) {
//        User owner = userRepository.findById(ownerId)
//                .orElseThrow(() -> new NotFoundException("Пользователь с таким ID не найден: " + ownerId));
//        Page<ProfileBookingInfoDTO> profileBookingsPaginated = bookingRepository.getOwnerBookingsInfo(owner, pageable);
//        List<ProfileBookingInfoDTO> result = profileBookingsPaginated.getContent();
//        return new PageImpl<>(result, pageable, profileBookingsPaginated.getTotalElements());
//    }

    @Override
    public BookingDTO create (BookingDTO bookingDTO) {
        UserDTO guestDTO = userService.getOne(bookingDTO.getGuestId());
        StayDTO stayDTO = stayService.getOne(bookingDTO.getStayId());
        UserDTO ownerDTO = userService.getOne(stayDTO.getOwnerId());
        BookingDTO newBooking = mapper.toDTO(repository.save(mapper.toEntity(bookingDTO)));
        mailService.sendSuccessfulBookingEmail(bookingDTO, guestDTO, ownerDTO, stayDTO);
        return newBooking;
    }

    public Page<ProfileBookingInfoDTO> getBookingsInfo(Long userId, Pageable pageable) {
        User user = userMapper.toEntity(userService.getOne(userId));
        List<ProfileBookingInfoDTO> result = new ArrayList<>();
        Page<ProfileBookingInfoDTO> profileBookingsPaginated = new PageImpl<>(result);
        if (user.getRole().getId().equals(1L)) {
            profileBookingsPaginated = bookingRepository.getGuestBookingsInfo(user, pageable);
        } else if (user.getRole().getId().equals(2L)) {
            profileBookingsPaginated = bookingRepository.getOwnerBookingsInfo(user, pageable);
        }
        result = profileBookingsPaginated.getContent();
        return new PageImpl<>(result, pageable, profileBookingsPaginated.getTotalElements());
    }


    public void cancel(Long bookingId) throws CancelBookingException {
        CustomUserDetails customUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        BookingDTO bookingDTO = mapper.toDTO(bookingRepository.findById(bookingId)
                .orElseThrow(() -> new NotFoundException("Бронирование с таким ID не найдено: " + bookingId)));
        Long securityId = Long.valueOf(customUser.getUserId());
        Long ownerId = stayService.getOne(bookingDTO.getStayId()).getOwnerId();
        Long guestId = bookingDTO.getGuestId();

        //if the user is NOT A CAT OWNER, they may only cancel the booking 1 day in advance
        if (Collections.disjoint(customUser.getAuthorities(),
                List.of(new SimpleGrantedAuthority("ROLE_" + UserRoleConstants.CAT_OWNER)))) {
            if (ownerId.equals(securityId)) {
                if (LocalDate.now().isBefore(bookingDTO.getDateBegin()))
                    bookingDTO.setCanceled(true);
                else {
                    throw new CancelBookingException(String.format(Errors.Bookings.BOOKING_CANNOT_BE_DELETED, 1, "день"));
                }
            }
        } else {
            if (guestId.equals(securityId)) {
                //if the user IS A CAT OWNER, they may only cancel the booking 3 days in advance
                if (bookingDTO.getDateBegin().isAfter(LocalDate.now().plusDays(2))) {
                    bookingDTO.setCanceled(true);
                } else {
                    throw new CancelBookingException(String.format(Errors.Bookings.BOOKING_CANNOT_BE_DELETED, 3, "дня"));
                }
            }
        }
        bookingRepository.save(mapper.toEntity(bookingDTO));
    }

}
