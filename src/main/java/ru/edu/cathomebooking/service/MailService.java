package ru.edu.cathomebooking.service;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.edu.cathomebooking.constants.MailConstants;
import ru.edu.cathomebooking.dto.BookingDTO;
import ru.edu.cathomebooking.dto.StayDTO;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.utils.MailUtils;

import java.util.UUID;

import static ru.edu.cathomebooking.constants.FormatterConstant.FORMATTER_TO_USER;

@Service
public class MailService {
    private final JavaMailSender javaMailSender;

    public MailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public boolean sendSuccessfulBookingEmail(BookingDTO bookingDTO, UserDTO guest, UserDTO owner, StayDTO stay) {

        String guestEmail = guest.getEmail();
        String stayOwnerEmail = owner.getEmail();
        String dateBegin = bookingDTO.getDateBegin().format(FORMATTER_TO_USER);
        String dateEnd = bookingDTO.getDateEnd().format(FORMATTER_TO_USER);

        SimpleMailMessage mailMessageForGuest = MailUtils.createEmailMessage(MailConstants.BOOKING_CONFIRMATION_SUBJECT,
                String.format(MailConstants.BOOKING_CONFIRMATION_TEXT,
                        guest.getFirstName(),
                        stay.getName(),
                        stay.getAddress(),
                        dateBegin,
                        dateEnd,
                        owner.getFirstName(),
                        owner.getPhone()),
                guestEmail);
        javaMailSender.send(mailMessageForGuest);

        SimpleMailMessage mailMessageForOwner = MailUtils.createEmailMessage(MailConstants.BOOKING_CONFIRMATION_SUBJECT,
                String.format(MailConstants.NEW_BOOKING_TEXT,
                        owner.getFirstName(),
                        stay.getName(),
                        stay.getAddress(),
                        dateBegin,
                        dateEnd,
                        guest.getFirstName(),
                        guest.getPhone()),
                stayOwnerEmail);
        javaMailSender.send(mailMessageForOwner);
        return true;
    }

    public void sendChangePasswordEmail(final UserDTO userDTO, UUID uuid) {
        SimpleMailMessage mailMessage = MailUtils.createEmailMessage(MailConstants.REMEMBER_PASSWORD_SUBJECT,
                MailConstants.REMEMBER_PASSWORD_TEXT + uuid,
                userDTO.getEmail());
        javaMailSender.send(mailMessage);
    }
}
