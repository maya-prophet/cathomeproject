package ru.edu.cathomebooking.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.edu.cathomebooking.dto.PriceDTO;
import ru.edu.cathomebooking.mapper.PriceMapper;
import ru.edu.cathomebooking.model.Price;
import ru.edu.cathomebooking.repository.PriceRepository;

import java.util.List;

@Slf4j
@Service
public class PriceService extends GenericService<Price, PriceDTO> {

    private final PriceRepository priceRepository;
    protected PriceService(PriceRepository repository,
                           PriceMapper mapper) {
        super(repository, mapper);
        this.priceRepository = repository;
    }


    public Page<PriceDTO> getStayPrices(Long stayId, Pageable pageable) {
        Page<Price> pricesPaginated = priceRepository.getAllByStayId(stayId, pageable);
        List<PriceDTO> result = mapper.toDTOs(pricesPaginated.getContent());

        return new PageImpl<>(result, pageable, pricesPaginated.getTotalElements());
    }

    public List<PriceDTO> getStayPrices(Long stayId) {
        return mapper.toDTOs(priceRepository.getAllByStayId(stayId));
    }

    public boolean checkIfValidPeriod(PriceDTO price, Long stayId) {
        return priceRepository.validatePeriod(price.getFromDate(), price.getToDate(), stayId);
    }

    public void updateDefaultPrice(PriceDTO defaultPrice, Long stayId) {
        log.info("Default price stay id: {}", defaultPrice.getStayId());
        List<PriceDTO> prices = getStayPrices(stayId);
        boolean foundDefault = false;
        if (prices != null && !prices.isEmpty()) {
            for (PriceDTO price : prices) {
                if (price.getFromDate() == null && price.getToDate() == null) {
                    foundDefault = true;
                    price.setPrice(defaultPrice.getPrice());
                    update(price);
                }
            }
        }
        if (!foundDefault) {
            defaultPrice.setStayId(stayId);
            create(defaultPrice);
        }
    }
}
