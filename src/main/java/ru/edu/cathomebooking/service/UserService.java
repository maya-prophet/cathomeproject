package ru.edu.cathomebooking.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.cathomebooking.dto.RoleDTO;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.mapper.UserMapper;
import ru.edu.cathomebooking.model.User;
import ru.edu.cathomebooking.repository.UserRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class UserService extends GenericService<User, UserDTO> {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final MailService mailService;

    public UserService(UserMapper mapper,
                       UserRepository userRepository,
                       MailService mailService,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(userRepository, mapper);
        this.userRepository = userRepository;
        this.mailService = mailService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(object.getRoleId());
        object.setRole(roleDTO);
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }


    public UserDTO getUserByLogin(String login) {
        return mapper.toDTO(userRepository.findUserByLogin(login));
    }

    public UserDTO getUserByEmail(String email) {
        return mapper.toDTO(userRepository.findUserByEmail(email));
    }


    public List<String> getGuestEmailsWithUpcomingBookings(LocalDate referenceDate) {
        return userRepository.getEmailsForUpcomingBookings(referenceDate);
    }

    public void sendChangePasswordEmail(final UserDTO userDTO) {
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        update(userDTO);
        mailService.sendChangePasswordEmail(userDTO, uuid);
    }

    public void changePassword(final String uuid,
                               final String password) throws NotFoundException {
        UserDTO user = mapper.toDTO(userRepository.findByChangePasswordToken(uuid));
        if (user == null) {
            throw new NotFoundException("Пользователь с таким токеном не найден. " +
                    "Возможно, пользователь повторно прошел по старой ссылке восстановления пароля.");
        }
        user.setPassword(bCryptPasswordEncoder.encode(password));
        user.setChangePasswordToken(null);
        update(user);
    }

    //для REST
    public boolean checkPassword(String password, UserDetails userDetails) {
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }
}
