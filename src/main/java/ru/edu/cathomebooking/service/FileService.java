package ru.edu.cathomebooking.service;

import io.minio.*;
import io.minio.messages.Item;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.edu.cathomebooking.dto.StayDTO;
import ru.edu.cathomebooking.utils.MinioURLUtils;

import java.io.InputStream;
import java.util.*;

@Service
@Slf4j
public class FileService {

    private final MinioClient minioClient;
    private final StayService stayService;

    @Value("${minio.bucket.name}")
    private String bucketName;

    public FileService(MinioClient minioClient, StayService stayService) {
        this.minioClient = minioClient;
        this.stayService = stayService;
    }
    public List<String> getFileList(String subdirectory, Long id) {
        List<String> fileUrls = new ArrayList<>();
        try {
            Iterable<Result<Item>> result = minioClient.listObjects(ListObjectsArgs.builder()
                    .bucket(bucketName)
                    .recursive(true)
                    .build());
            for (Result<Item> item : result) {
                if (item.get().objectName().startsWith(subdirectory + "/" + id))
                    fileUrls.add(MinioURLUtils.getPreSignedUrl(item.get().objectName()));
            }
            return fileUrls;
        } catch (Exception e) {
            log.error("MinioService#getFileList: ошибка при получении списка файлов", e);
        }

        return fileUrls;
    }

    public byte[] getFile(String subdirectory, Long id, String filename) {
        byte[] file;
        try (
                InputStream stream = minioClient.getObject(GetObjectArgs.builder()
                .bucket(bucketName)
                .object("/" + subdirectory + "/" + id + "/" + filename)
                .build())
        ){
            file = IOUtils.toByteArray(stream);
        } catch (Exception ex) {
            log.error("MinioService#getFile: ошибка при получении файла", ex);
            return null;
        }
        return file;
    }

    public boolean uploadFile(MultipartFile file, String subdirectory, Long id) {
        try (InputStream stream = file.getInputStream()) {
            minioClient.putObject(PutObjectArgs.builder()
                    .bucket(bucketName)
                    .object("/" + subdirectory + "/" + id + "/" + file.getOriginalFilename())
                    .stream(stream, file.getSize(), -1)
                    .build());
        } catch (Exception ex) {
            log.error("MinioService#uploadFile: ошибка при загрузке файла", ex);
            return false;
        }
        String url = MinioURLUtils.getPreSignedUrl("stay", id, file.getOriginalFilename());
        StayDTO stayDTO = stayService.getOne(id);
        stayDTO.getPhotoUrls().add(url);
        stayService.update(stayDTO);
        return true;
    }

}
