package ru.edu.cathomebooking.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public class MinioURLUtils {

    private static final String DEFAULT_DIRECTORY = "http://localhost:8092/file/";

    private MinioURLUtils(){}

    public static String getPreSignedUrl(String filename) {

        return DEFAULT_DIRECTORY + filename;
    }

    public static String getPreSignedUrl(String subdirectory, Long id, String filename) {

        return DEFAULT_DIRECTORY +
                subdirectory +
                "/" +
                id +
                "/" +
                filename;
    }
}
