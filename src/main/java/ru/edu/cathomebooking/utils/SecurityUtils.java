package ru.edu.cathomebooking.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import ru.edu.cathomebooking.service.userdetails.CustomUserDetails;

public class SecurityUtils {

    private SecurityUtils() {
    }

    public static boolean checkUser(final Long userId) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder
                                                                        .getContext()
                                                                        .getAuthentication()
                                                                        .getPrincipal();
        return userId.equals(Long.valueOf(customUserDetails.getUserId()));
    }
}
