package ru.edu.cathomebooking.utils;

import org.springframework.mail.SimpleMailMessage;

public class MailUtils {

    private MailUtils() {}

    public static SimpleMailMessage createEmailMessage(final String subject,
                                                       final String text,
                                                       final String... emails) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(emails);
        message.setSubject(subject);
        message.setText(text);
        return message;
    }

}
