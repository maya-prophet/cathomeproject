package ru.edu.cathomebooking.constants;

import java.util.List;

public interface WebSecurityConstants {

    List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
                                                "/js/**",
                                                "/scss/**",
                                                "/css/**",
                                                "/img/**",
                                                "/lib/**",
                                                "/v3/api-docs/**",
                                                "/swagger-ui/**",
                                                "/",
                                                "/in-development");


    List<String> STAYS_WHITE_LIST = List.of("/stays",
                                            "/stays/**",
                                            "/stays/findAvailableStays",
                                            "/stays/availableStays");

    List<String> STAYS_OWNER_PERMISSION_LIST = List.of("/stays/add",
                                                       "/stays/update/**",
                                                       "/stays/delete/**",
                                                       "/stays/owner-stays");

    List<String> BOOKINGS_GUEST_PERMISSION_LIST = List.of("/bookings/book",
                                                          "/bookings/bookingRequest",
                                                          "/bookings/success");

    List<String> PRICES_OWNER_PERMISSION_LIST = List.of("/prices/**",
                                                        "/prices/update/**",
                                                        "/prices/update/default/**",
                                                        "/prices/delete/**");

    List<String> USERS_WHITE_LIST = List.of("/login",
                                            "/users/registration",
                                            "/users/remember-password",
                                            "/users/change-password");

    List<String> USER_PERMISSION_LIST = List.of("/users/profile",
                                                  "/users/update-user-info");

    List<String> FILES_WHITE_LIST = List.of("/file",
                                            "/file/stay/**");

    List<String> REST_USERS_WHITE_LIST = List.of("/rest/users/auth");
    List<String> REST_STAYS_OWNER_PERMISSION_LIST = List.of("/rest/stays/add",
            "/rest/stays/update/**",
            "/rest/stays/delete/**",
            "/rest/stays/owner-stays");
}
