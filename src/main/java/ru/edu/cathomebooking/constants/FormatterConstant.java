package ru.edu.cathomebooking.constants;

import java.time.format.DateTimeFormatter;

public interface FormatterConstant {

    DateTimeFormatter FORMATTER_TO_DB = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    DateTimeFormatter FORMATTER_TO_USER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
}
