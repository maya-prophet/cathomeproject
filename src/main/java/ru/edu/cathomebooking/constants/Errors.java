package ru.edu.cathomebooking.constants;

public interface Errors {

    class Stays {
        public static final String STAY_DELETE_ERROR = """
                                                           Это место размещения не может быть удалено, поскольку с ним 
                                                           есть активные бронирования
                                                       """;
    }

    class Prices {
        public static final String PRICE_PERIOD_OVERLAP_ERROR = """
                                                                    Новый ценовой период пересекается со старыми
                                                                    или имеет ошибку
                                                                """;
    }

    class Bookings {
        public static final String BOOKING_CANNOT_BE_DELETED = """
                                                                  Бронирование может быть отменено не позднее, чем за
                                                                  %s %s до его начала
                                                               """;
    }

}
