package ru.edu.cathomebooking.constants;

public interface UserRoleConstants {

    String ADMIN = "ADMIN";

    String CAT_OWNER = "CAT_OWNER";

    String STAY_OWNER = "STAY_OWNER";
}
