package ru.edu.cathomebooking.constants;

public interface QueryConstants {
        String AVAILABLE_STAYS_WITH_AVAILABLE_SPOTS = """
            SELECT s.id AS id, 
                   s.stay_name AS name, 
                   s.city AS city, 
                   s.address AS address, 
                   s.longitude AS longitude, 
                   s.latitude AS latitude, 
                   s.spot_amount AS spotAmount, 
                   s.owner_id AS owner, 
                   s.stay_type AS type, 
                   s.description AS description, 
                   s.has_toys AS hasToys, 
                   s.has_scratching_post AS hasScratchingPost, 
                   s.has_ind_tray AS hasIndTray, 
                   s.has_video AS hasVideo, 
                   s.has_food AS hasFood, 
                   s.individual_stay as individualStay, 
                   s.photo_urls AS photoUrls, 
                   :catAmount * sum(COALESCE(p.price, dp.price)) AS totalCost
            FROM stays s LEFT JOIN (SELECT
                                        COALESCE((TO_DATE(:dateBegin, 'YYYY-MM-DD') + (Begin.X + 1)), TO_DATE(:dateBegin, 'YYYY-MM-DD')) AS date
                                    FROM
                                        (SELECT
                                                 B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
                                         FROM
                                             (SELECT 0 AS X UNION SELECT 1) AS B0,
                                             (SELECT 0 AS X UNION SELECT 1) AS B1,
                                             (SELECT 0 AS X UNION SELECT 1) AS B2,
                                             (SELECT 0 AS X UNION SELECT 1) AS B3,
                                             (SELECT 0 AS X UNION SELECT 1) AS B4,
                                             (SELECT 0 AS X UNION SELECT 1) AS B5
                                         WHERE
                                                     B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < TO_DATE(:dateEnd, 'YYYY-MM-DD') - TO_DATE(:dateBegin, 'YYYY-MM-DD')) AS Begin
                                            FULL JOIN (SELECT
                                                               B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
                                                       FROM
                                                           (SELECT 0 AS X UNION SELECT 1) AS B0,
                                                           (SELECT 0 AS X UNION SELECT 1) AS B1,
                                                           (SELECT 0 AS X UNION SELECT 1) AS B2,
                                                           (SELECT 0 AS X UNION SELECT 1) AS B3,
                                                           (SELECT 0 AS X UNION SELECT 1) AS B4,
                                                           (SELECT 0 AS X UNION SELECT 1) AS B5
                                                       WHERE
                                                                   B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < TO_DATE(:dateEnd, 'YYYY-MM-DD') - TO_DATE(:dateBegin, 'YYYY-MM-DD')) AS Ending
                                                      ON (Begin.X + 1 = Ending.X)
                                    ORDER BY date) AS d ON TRUE
                        
                         LEFT JOIN prices p
                                ON s.id = p.stay_id and d.date BETWEEN p.from_date AND p.to_date
                         LEFT JOIN (SELECT stay_id, price FROM prices
                                    WHERE from_date IS NULL AND to_date IS NULL) AS dp
                                ON s.id = dp.stay_id
            LEFT JOIN (SELECT b.stay_id, sum(b.cat_amount) as occupied_spots
                         FROM bookings b
                        WHERE (:dateBegin BETWEEN b.date_begin AND b.date_end)
                                OR (:dateEnd BETWEEN b.date_begin AND b.date_end)
                                OR :dateBegin < b.date_begin AND :dateEnd > b.date_end
                          AND b.is_canceled = false
                        GROUP BY b.stay_id) AS stays_with_occupied_spots
                   ON s.id = stays_with_occupied_spots.stay_id
                WHERE s.city like COALESCE(:city, '%')
                  AND s.is_deleted = false
                  AND s.spot_amount >= COALESCE(stays_with_occupied_spots.occupied_spots, 0) + :catAmount
             GROUP BY s.id 
             """;

    String AVAILABLE_STAYS_WITH_AVAILABLE_SPOTS_COUNT = """
            SELECT count(*) FROM 
            (SELECT s.id AS id, 
                   s.stay_name AS name, 
                   s.city AS city, 
                   s.address AS address, 
                   s.longitude AS longitude, 
                   s.latitude AS latitude, 
                   s.spot_amount AS spotAmount, 
                   s.owner_id AS owner, 
                   s.stay_type AS type, 
                   s.description AS description, 
                   s.has_toys AS hasToys, 
                   s.has_scratching_post AS hasScratchingPost, 
                   s.has_ind_tray AS hasIndTray, 
                   s.has_video AS hasVideo, 
                   s.has_food AS hasFood, 
                   s.individual_stay as individualStay, 
                   s.photo_urls AS photoUrls, 
                   :catAmount * sum(COALESCE(p.price, dp.price)) AS totalCost
            FROM stays s LEFT JOIN (SELECT
                                        COALESCE((TO_DATE(:dateBegin, 'YYYY-MM-DD') + (Begin.X + 1)), TO_DATE(:dateBegin, 'YYYY-MM-DD')) AS date
                                    FROM
                                        (SELECT
                                                 B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
                                         FROM
                                             (SELECT 0 AS X UNION SELECT 1) AS B0,
                                             (SELECT 0 AS X UNION SELECT 1) AS B1,
                                             (SELECT 0 AS X UNION SELECT 1) AS B2,
                                             (SELECT 0 AS X UNION SELECT 1) AS B3,
                                             (SELECT 0 AS X UNION SELECT 1) AS B4,
                                             (SELECT 0 AS X UNION SELECT 1) AS B5
                                         WHERE
                                                     B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < TO_DATE(:dateEnd, 'YYYY-MM-DD') - TO_DATE(:dateBegin, 'YYYY-MM-DD')) AS Begin
                                            FULL JOIN (SELECT
                                                               B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
                                                       FROM
                                                           (SELECT 0 AS X UNION SELECT 1) AS B0,
                                                           (SELECT 0 AS X UNION SELECT 1) AS B1,
                                                           (SELECT 0 AS X UNION SELECT 1) AS B2,
                                                           (SELECT 0 AS X UNION SELECT 1) AS B3,
                                                           (SELECT 0 AS X UNION SELECT 1) AS B4,
                                                           (SELECT 0 AS X UNION SELECT 1) AS B5
                                                       WHERE
                                                                   B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < TO_DATE(:dateEnd, 'YYYY-MM-DD') - TO_DATE(:dateBegin, 'YYYY-MM-DD')) AS Ending
                                                      ON (Begin.X + 1 = Ending.X)
                                    ORDER BY date) AS d ON TRUE
                        
                         LEFT JOIN prices p
                                ON s.id = p.stay_id and d.date BETWEEN p.from_date AND p.to_date
                         LEFT JOIN (SELECT stay_id, price FROM prices
                                    WHERE from_date IS NULL AND to_date IS NULL) AS dp
                                ON s.id = dp.stay_id
            LEFT JOIN (SELECT b.stay_id, sum(b.cat_amount) as occupied_spots
                         FROM bookings b
                        WHERE (:dateBegin BETWEEN b.date_begin AND b.date_end)
                                OR (:dateEnd BETWEEN b.date_begin AND b.date_end)
                                OR :dateBegin < b.date_begin AND :dateEnd > b.date_end
                          AND b.is_canceled = false
                        GROUP BY b.stay_id) AS stays_with_occupied_spots
                   ON s.id = stays_with_occupied_spots.stay_id
                WHERE s.city like COALESCE(:city, '%')
                  AND s.is_deleted = false
                  AND s.spot_amount >= COALESCE(stays_with_occupied_spots.occupied_spots, 0) + :catAmount
             GROUP BY s.id) t""";

    String GUEST_PROFILE_BOOKINGS_INFO = """
            SELECT new ru.edu.cathomebooking.dto.ProfileBookingInfoDTO( b.id,
                                                                        b.dateBegin, 
                                                                        b.dateEnd, 
                                                                        b.catAmount, 
                                                                        b.totalCost, 
                                                                        s.id,
                                                                        s.name, 
                                                                        s.city, 
                                                                        s.address, 
                                                                        b.isCanceled
                                                                        ) 
            FROM Booking b JOIN b.stay AS s
            WHERE b.guest = :guest
            ORDER BY b.isCanceled, b.dateBegin DESC, b.dateEnd DESC""";

    String OWNER_PROFILE_BOOKINGS_INFO = """
            SELECT new ru.edu.cathomebooking.dto.ProfileBookingInfoDTO( b.id,
                                                                        b.dateBegin, 
                                                                        b.dateEnd, 
                                                                        b.catAmount, 
                                                                        b.totalCost,
                                                                        s.id, 
                                                                        s.name, 
                                                                        s.city, 
                                                                        s.address, 
                                                                        b.isCanceled
                                                                        ) 
            FROM Booking b JOIN b.stay AS s
            WHERE s.owner = :owner
            ORDER BY b.isCanceled, b.dateBegin DESC, b.dateEnd DESC""";
}
