package ru.edu.cathomebooking.constants;

public interface MailConstants {

    String REMEMBER_PASSWORD_SUBJECT = "Восстановление пароля на сайте КошкинДом";
    String REMEMBER_PASSWORD_TEXT = """
                    Добрый день. Вы получили это письмо, так как с вашего аккаунта была отправлена заявка на восстановление пароля.
                    Для восстановления пароля перейдите по ссылке: http://localhost:8092/users/change-password?uuid=""";

    String BOOKING_REMINDER_SUBJECT = "Напоминание о предстоящем бронировании";
    String BOOKING_REMINDER_TEXT = "Добрый день. Завтра начинается Ваше бронирование.";

    String BOOKING_CONFIRMATION_SUBJECT = "Бронирование на сайте КошкинДом";

    String BOOKING_CONFIRMATION_TEXT = "Здравствуйте, %s.%nВаше брониронвание в %s подтверждено.%n" +
            "Вас будут ждать по адресу %s.%nПериод бронирования с %s по %s.%n" +
            "Вы можете связаться с владельцем места размещения по телефону: %s, %s";

    String NEW_BOOKING_TEXT = "Здравствуйте, %s.%nБыло получено новое бронирование в %s " +
            "по адресу %s.%nПериод бронирования с %s по %s.%n" +
            "Вы можете связаться с которазмещателем по телефону: %s, %s";


}
