package ru.edu.cathomebooking.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "bookings")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "bookings_seq", allocationSize = 1)
//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Booking extends GenericModel {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "stay_id", foreignKey = @ForeignKey(name = "FK_BOOKINGS_STAY"), nullable = false)
    private Stay stay;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "guest_id", foreignKey = @ForeignKey(name = "FK_BOOKINGS_GUEST"), nullable = false)
    private User guest;

    @Column(name = "date_begin", nullable = false)
    private LocalDate dateBegin;

    @Column(name = "date_end", nullable = false)
    private LocalDate dateEnd;

    @Column(name = "cat_amount", nullable = false)
    private Integer catAmount;

    @Column(name = "total_cost", nullable = false)
    private BigDecimal totalCost;

    @Column(name = "is_canceled", columnDefinition = "boolean default false")
    private boolean isCanceled = false;
}
