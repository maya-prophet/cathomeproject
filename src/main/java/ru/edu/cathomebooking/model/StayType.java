package ru.edu.cathomebooking.model;

public enum StayType {

    DEFAULT("Не указано"),
    IN_HOME_BOARDING("Временная семья"),
    HOTEL("Зоогостиница");


    private final String typeName;
    StayType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }

//    public static StayType fromNumber(int num) {
//        if (num < 0 || num >= StayType.values().length) return DEFAULT;
//        return StayType.values()[num];
//    }
//
//    public static Short toNumber(StayType type) {
//        StayType[] values = StayType.values();
//        Short res = 0;
//        for (short i = 0; i < values.length; i++) {
//            if (type == values[i])
//                res = i;
//        }
//        return res;
//    }

}
