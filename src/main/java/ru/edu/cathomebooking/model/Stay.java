package ru.edu.cathomebooking.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.persistence.*;
import lombok.*;
import ru.edu.cathomebooking.converter.UrlsListConverter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "stays", uniqueConstraints = @UniqueConstraint(name = "uniqueName", columnNames = "stay_name"))
@SequenceGenerator(name = "default_gen", sequenceName = "stays_seq", allocationSize = 1)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Stay extends GenericModel {

    @Column(name = "stay_name", nullable = false)
    private String name;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "longitude")
    private Double longitude;

    //возможно не пригодится
    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "spot_amount", nullable = false)
    private Integer spotAmount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", foreignKey = @ForeignKey(name = "FK_STAY_OWNER"), nullable = false)
    private User owner;

    @Column(name = "stay_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private StayType type;

    @Column(name = "description")
    private String description;

    @Column(name = "has_toys", columnDefinition = "boolean default false")
    private Boolean hasToys = false;

    @Column(name = "has_scratching_post", columnDefinition = "boolean default false")
    private Boolean hasScratchingPost = false;

    @Column(name = "has_ind_tray", columnDefinition = "boolean default false")
    private Boolean hasIndTray = false;

    @Column(name = "has_video", columnDefinition = "boolean default false")
    private Boolean hasVideo = false;

    @Column(name = "has_food", columnDefinition = "boolean default false")
    private Boolean hasFood = false;

    @Column(name = "individual_stay", columnDefinition = "boolean default false")
    private Boolean isIndividualStay = false;

//    @Type(ListArrayType.class,
//    columnDefinition = "text[]")
    @Column(name = "photo_urls", columnDefinition = "text")
    @Convert(converter = UrlsListConverter.class)
    private List<String> photoUrls = new ArrayList<>();


    @OneToMany(mappedBy = "stay", cascade = CascadeType.ALL)
    private List<Price> prices =  new ArrayList<>();

    @OneToMany(mappedBy = "stay", cascade = CascadeType.ALL)
    private List<Booking> bookings = new ArrayList<>();

    @Column(name = "is_deleted", columnDefinition = "boolean default false")
    private boolean isDeleted;

    @Column(name = "deleted_when")
    private LocalDateTime deletedWhen;

    @Column(name = "deleted_by")
    private String deletedBy;

}
