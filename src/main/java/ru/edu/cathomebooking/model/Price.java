package ru.edu.cathomebooking.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import jakarta.validation.constraints.FutureOrPresent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "prices")
@SequenceGenerator(name = "default_gen", sequenceName = "prices_seq", allocationSize = 1)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Price extends GenericModel {
    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "stay_id", foreignKey = @ForeignKey(name = "FK_PRICES_STAY"), nullable = false)
    private Stay stay;

    @Column(name = "from_date")
    private LocalDate fromDate;

    @Column(name = "to_date")
    private LocalDate toDate;

    @Column(name = "price", nullable = false)
    private BigDecimal price;

}
