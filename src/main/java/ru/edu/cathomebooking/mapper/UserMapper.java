package ru.edu.cathomebooking.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.edu.cathomebooking.dto.UserDTO;
import ru.edu.cathomebooking.model.User;
import ru.edu.cathomebooking.repository.BookingRepository;
import ru.edu.cathomebooking.repository.StayRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static ru.edu.cathomebooking.constants.FormatterConstant.FORMATTER_TO_DB;

@Component
public class UserMapper extends GenericMapper<User, UserDTO>{

    private final StayRepository stayRepository;
    private final BookingRepository bookingRepository;

    protected UserMapper(ModelMapper modelMapper,
                         StayRepository stayRepository,
                         BookingRepository bookingRepository) {
        super(modelMapper, User.class, UserDTO.class);
        this.stayRepository = stayRepository;
        this.bookingRepository = bookingRepository;
    }

    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setStayIds)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(UserDTO::setBookingIds)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(UserDTO::setBirthDate)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setStays)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip((User::setBookings))).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setBirthDate)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if(Objects.nonNull(source.getStayIds())) {
            destination.setStays(stayRepository.findAllById(source.getStayIds()));
        } else {
            destination.setStays(null);
        }

        if(Objects.nonNull(source.getBookingIds())) {
            destination.setBookings(bookingRepository.findAllById(source.getBookingIds()));
        } else {
            destination.setBookings(null);
        }
        if (!source.getBirthDate().equals(""))
            destination.setBirthDate(LocalDate.parse(source.getBirthDate(), FORMATTER_TO_DB));
        else destination.setBirthDate(null);
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        List<Long> stayIds = null;
        List<Long> bookingIds = null;
        if (Objects.nonNull(source)) {
            if (Objects.nonNull(source.getStays())) {
                stayIds = getIds(source.getStays());
            }
            if (Objects.nonNull(source.getBookings())) {
                bookingIds = getIds(source.getBookings());
            }
        }
        destination.setStayIds(stayIds);
        destination.setBookingIds(bookingIds);

        if (Objects.nonNull(source.getBirthDate()))
            destination.setBirthDate(source.getBirthDate().toString());
        else destination.setBirthDate("");
    }

}
