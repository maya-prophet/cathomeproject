package ru.edu.cathomebooking.mapper;

import ru.edu.cathomebooking.dto.GenericDTO;
import ru.edu.cathomebooking.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);

    D toDTO(E entity);

    List<E> toEntities(List<D> dtoList);

    List<D> toDTOs(List<E> entityList);
}
