package ru.edu.cathomebooking.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import ru.edu.cathomebooking.dto.BookingDTO;
import ru.edu.cathomebooking.model.Booking;
import ru.edu.cathomebooking.repository.StayRepository;
import ru.edu.cathomebooking.repository.UserRepository;

@Component
public class BookingMapper extends GenericMapper<Booking, BookingDTO> {

    private final UserRepository userRepository;
    private final StayRepository stayRepository;
    protected BookingMapper(ModelMapper modelMapper,
                            UserRepository userRepository,
                            StayRepository stayRepository) {
        super(modelMapper, Booking.class, BookingDTO.class);
        this.userRepository = userRepository;
        this.stayRepository = stayRepository;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(Booking.class, BookingDTO.class)
                .addMappings(m -> m.skip(BookingDTO :: setGuestId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(BookingDTO :: setStayId)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(BookingDTO.class, Booking.class)
                .addMappings(m -> m.skip(Booking :: setGuest)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Booking :: setStay)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(BookingDTO source, Booking destination) {
        destination.setGuest(userRepository.findById(source.getGuestId())
                .orElseThrow(() -> new NotFoundException("Пользователь (гость) с таким ID не найден: " + source.getGuestId())));
        destination.setStay(stayRepository.findById(source.getStayId())
                .orElseThrow(() -> new NotFoundException("Место размещения с таким ID не найдено: " + source.getStayId())));
    }

    @Override
    protected void mapSpecificFields(Booking source, BookingDTO destination) {
        destination.setGuestId(source.getGuest().getId());
        destination.setStayId(source.getStay().getId());
    }
}
