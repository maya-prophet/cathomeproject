package ru.edu.cathomebooking.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import ru.edu.cathomebooking.dto.PriceDTO;
import ru.edu.cathomebooking.model.Price;
import ru.edu.cathomebooking.repository.StayRepository;

import java.util.Objects;

@Component
public class PriceMapper extends GenericMapper<Price, PriceDTO> {

    private final StayRepository stayRepository;
    protected PriceMapper(ModelMapper modelMapper, StayRepository stayRepository) {
        super(modelMapper, Price.class, PriceDTO.class);
        this.stayRepository = stayRepository;
    }

    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(Price.class, PriceDTO.class)
                .addMappings(m -> m.skip(PriceDTO::setStayId))
                .setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(PriceDTO.class, Price.class)
                .addMappings(m -> m.skip(Price::setStay))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(PriceDTO source, Price destination) {
        destination.setStay(stayRepository.findById(source.getStayId())
                .orElseThrow(() -> new NotFoundException("Место размещения с таким ID не найдено")));
    }

    @Override
    protected void mapSpecificFields(Price source, PriceDTO destination) {
        if (Objects.nonNull(source))
            destination.setStayId(source.getStay().getId());
        else destination.setStayId(null);
    }
}
