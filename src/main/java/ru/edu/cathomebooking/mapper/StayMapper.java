package ru.edu.cathomebooking.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import ru.edu.cathomebooking.dto.StayDTO;
import ru.edu.cathomebooking.model.Stay;
import ru.edu.cathomebooking.repository.BookingRepository;
import ru.edu.cathomebooking.repository.UserRepository;

import java.util.List;
import java.util.Objects;

@Component
public class StayMapper extends GenericMapper<Stay, StayDTO> {

    private final BookingRepository bookingRepository;
//    private final UserRepository userRepository;

    protected StayMapper(ModelMapper modelMapper,
                         BookingRepository bookingRepository) {
//                         UserRepository userRepository) {
        super(modelMapper, Stay.class, StayDTO.class);
        this.bookingRepository = bookingRepository;
//        this.userRepository = userRepository;
    }

    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(Stay.class, StayDTO.class)
                .addMappings(m -> m.skip(StayDTO::setOwnerId))
                .setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(StayDTO::setBookingIds))
                .setPostConverter(toDTOConverter());


        modelMapper.createTypeMap(StayDTO.class, Stay.class)
                .addMappings(m -> m.skip(Stay::setBookings))
                .setPostConverter(toEntityConverter());
//                .addMappings(m -> m.skip(Stay::setOwner))
//                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(StayDTO source, Stay destination) {
//        if(Objects.nonNull(source.getOwnerId())) {
//            destination.setOwner(userRepository.findById(source.getOwnerId())
//                    .orElseThrow(() -> new NotFoundException("Пользователя с таким ID не найдено")));
//        } else {
//            destination.setOwner(null);
//        }
        if(Objects.nonNull(source.getBookingIds())) {
            destination.setBookings(bookingRepository.findAllById(source.getBookingIds()));
        } else {
            destination.setBookings(null);
        }
    }


    @Override
    protected void mapSpecificFields(Stay source, StayDTO destination) {
        List<Long> bookingIds = null;
        if(Objects.nonNull(source)) {
            destination.setOwnerId(source.getOwner().getId());
            if (Objects.nonNull(source.getBookings())) {
                bookingIds = getIds(source.getBookings());
            }
        }
        else destination.setOwnerId(null);
        destination.setBookingIds(bookingIds);
    }

    //1) Not able to skip owner., because there are already nested properties are mapped:
    // [owner.birthDate. owner.bookings. owner.changePasswordToken. owner.city. owner.email.
    // owner.firstName. owner.id. owner.lastName. owner.login. owner.middleName. owner.password.
    // owner.phone. owner.role.description. owner.role.id. owner.role.title. owner.stays.].
    // Do you skip the property after the implicit mappings mapped?
    // We recommended you to create an empty type map, and followed by addMappings and implicitMappings
    // calls

}
