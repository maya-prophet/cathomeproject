package ru.edu.cathomebooking.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.edu.cathomebooking.constants.MailConstants;
import ru.edu.cathomebooking.service.UserService;
import ru.edu.cathomebooking.utils.MailUtils;

import java.time.LocalDate;
import java.util.List;

@Component
@Slf4j
public class MailScheduler {

    private UserService userService;
    private JavaMailSender javaMailSender;

    public MailScheduler(UserService userService, JavaMailSender javaMailSender) {
        this.userService = userService;
        this.javaMailSender = javaMailSender;
    }

    @Autowired
    public UserService getUserService() {
        return userService;
    }

    @Autowired
    public JavaMailSender getJavaMailSender() {
        return javaMailSender;
    }

    //отправляем письмо за день до начала брони
    //    @Scheduled(cron = "0 0/1 * 1/1 * *") //каждую минуту
    @Scheduled(cron = "0 0 11 * * ?") // каждый день в 11 утра
    public void sendMailsBookingReminder() {
        List<String> emails = userService.getGuestEmailsWithUpcomingBookings(LocalDate.now().plusDays(1));
        if (emails.size() > 0) {
            SimpleMailMessage mailMessage = MailUtils.createEmailMessage(MailConstants.BOOKING_REMINDER_SUBJECT,
                                                                         MailConstants.BOOKING_REMINDER_TEXT,
                                                                         emails.toArray(new String[0]));
            javaMailSender.send(mailMessage);
        }
        log.info("Планировщик работает!");
    }
}
