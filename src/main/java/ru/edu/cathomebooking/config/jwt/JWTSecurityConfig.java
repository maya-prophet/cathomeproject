//package ru.edu.cathomebooking.config.jwt;
//
//import jakarta.servlet.http.HttpServletResponse;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
//import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.web.SecurityFilterChain;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//import org.springframework.security.web.firewall.HttpFirewall;
//import org.springframework.security.web.firewall.StrictHttpFirewall;
//import ru.edu.cathomebooking.service.userdetails.CustomUserDetailsService;
//import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
//
//import java.util.Arrays;
//
//import static ru.edu.cathomebooking.constants.UserRoleConstants.CAT_OWNER;
//import static ru.edu.cathomebooking.constants.UserRoleConstants.STAY_OWNER;
//import static ru.edu.cathomebooking.constants.WebSecurityConstants.*;
//
////для REST запросов
//@Configuration
//@EnableWebSecurity
//@EnableMethodSecurity
//public class JWTSecurityConfig {
//
//    private final CustomUserDetailsService customUserDetailsService;
//    private final JWTFilter jwtFilter;
//
//    public JWTSecurityConfig(CustomUserDetailsService customUserDetailsService, JWTFilter jwtFilter) {
//        this.customUserDetailsService = customUserDetailsService;
//        this.jwtFilter = jwtFilter;
//    }
//
//    @Bean
//    public HttpFirewall httpFirewall() {
//        StrictHttpFirewall firewall = new StrictHttpFirewall();
////        firewall.setAllowUrlEncodedPercent(true);
////        firewall.setAllowUrlEncodedSlash(true);
////        firewall.setAllowSemicolon(true);
//        firewall.setAllowedHttpMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));
//        return firewall;
//    }
//
//
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
//        return httpSecurity
//                .cors(AbstractHttpConfigurer::disable)
//                .csrf(AbstractHttpConfigurer::disable)
//                .authorizeHttpRequests(auth -> auth
//                        .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
//                        .requestMatchers(STAYS_WHITE_LIST.toArray(String[]::new)).permitAll()
//                        .requestMatchers(REST_USERS_WHITE_LIST.toArray(String[]::new)).permitAll()
//                        .requestMatchers(FILES_WHITE_LIST.toArray(String[]::new)).permitAll()
//                        .requestMatchers(REST_STAYS_OWNER_PERMISSION_LIST.toArray(String[]::new)).hasRole(STAY_OWNER)
//                        .requestMatchers(BOOKINGS_GUEST_PERMISSION_LIST.toArray(String[]::new)).hasRole(CAT_OWNER)
//                        .requestMatchers(USER_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(CAT_OWNER, STAY_OWNER)
//                        .anyRequest().authenticated())
//                .exceptionHandling()
//                .authenticationEntryPoint((request, response, authException) -> {
//                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
//                            authException.getMessage());
//                })
//                .and()
//                .sessionManagement(
//                        session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
//                //JWT Filter, valid or not
//                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class)
//                .userDetailsService(customUserDetailsService)
//                .build();
//    }
//
//    //обязательный бин, без которого работать не будет
//    @Bean
//    public AuthenticationManager authenticationManager(
//            AuthenticationConfiguration authenticationConfiguration) throws Exception {
//        return authenticationConfiguration.getAuthenticationManager();
//    }
//}
