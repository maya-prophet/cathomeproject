package ru.edu.cathomebooking.config.cache;

import lombok.*;
import ru.edu.cathomebooking.dto.AvailableStaysRequestDTO;

import java.time.LocalDate;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class StayRequestSessionCache {
    private String city;
    private Integer catAmount;
    private LocalDate dateBegin;
    private LocalDate dateEnd;

    public StayRequestSessionCache(AvailableStaysRequestDTO requestDTO) {
        this.city = requestDTO.getCity();
        this.catAmount = requestDTO.getCatAmount();
        this.dateBegin = requestDTO.getDateBegin();
        this.dateEnd = requestDTO.getDateEnd();
    }

    public boolean isEmpty() {
        return catAmount == null && dateBegin == null && dateEnd == null;
    }
}
