package ru.edu.cathomebooking.config.cache;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import ru.edu.cathomebooking.config.cache.StayRequestSessionCache;

@Configuration
public class SessionCacheConfig {

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION,
            proxyMode = ScopedProxyMode.TARGET_CLASS)
    public StayRequestSessionCache requestCache() {
        return new StayRequestSessionCache();
    }
}
