package ru.edu.cathomebooking.exception;

public class PricePeriodOverlapException extends Exception {
    public PricePeriodOverlapException(String message) {
        super(message);
    }
}
