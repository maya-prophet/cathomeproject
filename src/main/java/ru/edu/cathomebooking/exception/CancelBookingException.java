package ru.edu.cathomebooking.exception;

public class CancelBookingException extends Exception {
    public CancelBookingException(String message) {
        super(message);
    }
}
