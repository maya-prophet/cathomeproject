insert into roles
values (1, 'Роль котовладельца', 'CAT_OWNER'),
       (2, 'Роль арендодателя', 'STAY_OWNER');

insert into users (id, birth_date, city, email, first_name, last_name, login, password, phone, role_id)
values
(nextval('users_seq'), '1996-10-28', 'Балашиха', 'cburov@example.org',	'Юлий', 'Федотов', 'cburov', '$2y$10$O0gWVeWRpKhEghC6pTGLJuxmkch8uZTGl6yX5F.zPFGKWxddfPYou', '9631946041', 2),
(nextval('users_seq'), '2001-10-04', 'Луховицы', 'egalkina@example.net', 'Юрий', 'Тимофеев', 'egalkina', '$2y$10$D176TriIlYNALP5GfD4TFuMHNnh1Y1V4IY2PI8ZQyJ/5aOQQ.JUHq', '+7(952)2971536', 1),
(nextval('users_seq'), '1964-10-09', 'Пушкино', 'paksenov@example.org', 'Давид', 'Орлов', 'paksenov', '$2y$10$jglJ5QDUFmhyOz4sOEwkqODxSbrEIX5qOIGwkxWkl.PUenNH0wBEq', '9636876293', 1),
(nextval('users_seq'), '1969-03-16', 'Волоколамск', 'vgrisin@example.com', 'Савва', 'Поляков', 'vgrisin', '$2y$10$Zw3zCqAvzjNPtSzzjdi8eeskxCypFwvrKnlQBlQFCduXgeZ.Fx3fm', '+7(953)4413542', 2),
(nextval('users_seq'), '1999-02-19', 'Зарайск', 'ykazakova@example.com', 'Ростислав', 'Рябов', 'ykazakova', '$2y$10$.ScEXLMhT4c8xOhzuBvBIeNvFRhgMs7llyDiF00/6QC8Jo/acicB.', '+79648052282', 2),
(nextval('users_seq'), '2000-08-14', 'Лотошино', 'lihaceva.polina@example.com', 'Феликс', 'Орлов', 'lihaceva.polina', '$2y$10$gxVw.u.sosWbcQ2wiTPS8OJZsRHFE89LgfBkmjFnfw9tYDAlebjG6', '+79505798067', 2),
(nextval('users_seq'), '1964-08-03', 'Раменское', 'uvarov.dina@example.com', 'Иосиф', 'Щукин', 'uvarov.dina', '$2y$10$Rpk0lVJzVYaqATOd2.wf6.IbKhBHsEx5JYgidJAcr8oFfUj0FR4Sa', '+7(904)9565753', 2),
(nextval('users_seq'), '1971-03-31', 'Луховицы', 'matvej47@example.org', 'Марат', 'Матвеев', 'matvej47', '$2y$10$3HtvKrrs5eJyizV/WRA.O.8wbp67XESU.Pg6e/vPdIS.1RoE77kRG', '+79653616115', 1),
(nextval('users_seq'), '1966-06-09', 'Раменское', 'iosif91@example.net', 'Фёдор', 'Котов', 'iosif91', '$2y$10$8D0IhjS39zJEUZGOUhGkTeHv27EFb.6I4ATreWAEv17Z6vv3l0smy', '+79022774374', 2),
(nextval('users_seq'), '1955-09-06', 'Серпухов', 'vsamsonova@example.com', 'Ева', 'Князева', 'vsamsonova', '$2y$10$OB4Ghs84HEQiy7/plg/8wuYS.Upir5ZuN7LbG.Y1xN.sbOeQrR.BG', '+7(952)8270016', 1),
(nextval('users_seq'), '1978-11-08', 'Красногорск', 'vitalij.trofimov@example.net', 'Эдуард', 'Гришин', 'vitalij.trofimov', '$2y$10$UVtlz7.KbEoeUVU4QQ5R2ugjYaTqER1MsvYlbli5ngKlAsX.kZZom', '9517560111', 1),
(nextval('users_seq'), '2004-03-08', 'Сергиев Посад', 'innokentij.gusin@example.com', 'Давид', 'Филиппов', 'innokentij.gusin', '$2y$10$cgsUm2whkp4/YmiS6u7sv.MrfNrDCexiCG8BWgIq31BQerompghoC', '9518965988', 2),
(nextval('users_seq'), '1953-02-18', 'Лотошино', 'wpetrova@example.net', 'Мирослав', 'Гущин', 'wpetrova', '$2y$10$upOrZxDpkDyXTxTDYA3AUOeDYhEeOhis.baFu/RiYypT43T2lXqCq', '+79004731971', 1),
(nextval('users_seq'), '2003-03-31', 'Зарайск', 'anatolij34@example.net', 'Регина', 'Воронова', 'anatolij34', '$2y$10$hVQ4.AU4LzLwNkg/a416MerLdznqHjX9FKSAhEv94/a6QBJZg3Q4K', '+7(966)8520830', 2),
(nextval('users_seq'), '1974-01-02', 'Одинцово', 'pmerkuseva@example.net', 'Павел', 'Соколов', 'pmerkuseva', '$2y$10$vRnn9uv9fxH6/tF5s.CQEebZd2q.A6BTniUchlcVFo22L0xZz4l4S', '+79669811138', 1);

insert into stays (id, address, stay_name, spot_amount, stay_type, owner_id, city)
values
    (nextval('stays_seq'), 'г Москва, ул.Южнопортовая 5, стр.7, этаж 2, офис 212', 'Сэр Кот, м.Кожуховская', 20, 'HOTEL', 1, 'Москва'),
    (nextval('stays_seq'), 'г Москва, ул.Большая Очаковская 47а стр.1, этаж 8, офис 800', 'Сэр Кот, м.Озерная', 15, 'HOTEL', 1, 'Москва'),
    (nextval('stays_seq'), 'г Москва, ул.Алтуфьевское шоссе 37 стр.4, этаж 2, офис 234', 'Сэр Кот, м.Отрадное', 18, 'HOTEL', 4, 'Москва'),
    (nextval('stays_seq'), 'г Москва, ул. 3-я Хорошевская 13к1', 'Сэр Кот, м.Зорге', 15, 'HOTEL', 4, 'Москва'),
    (nextval('stays_seq'), 'г Санкт-Петербург, ул. Марата, дом 82, БЦ Комплект-Плюс', 'Booking Cat', 12, 'HOTEL', 5, 'Санкт-Петербург'),
    (nextval('stays_seq'), 'г Санкт-Петербург, Рябовское ш., 101', 'Cat Town, м.Ладожская', 20, 'HOTEL', 9, 'Санкт-Петербург'),
    (nextval('stays_seq'), 'г Санкт-Петербург, Ленинский пр., 139', 'Cat Town', 18, 'HOTEL', 9, 'Санкт-Петербург'),
    (nextval('stays_seq'), 'г Санкт-Петербург, Политехническая ул., 7 участок 5', 'Котвилль', 10, 'HOTEL', 12, 'Санкт-Петербург'),
    (nextval('stays_seq'), 'г Санкт-Петербург, ул. Марата, 77', 'Передержа на ул.Марата', 3, 'IN_HOME_BOARDING', 14, 'Санкт-Петербург'),
    (nextval('stays_seq'), 'г Москва, Политехническая ул., 8', 'Дома у Иосифа', 3, 'IN_HOME_BOARDING', 7, 'Москва');

update stays
    set photo_urls = 'http://localhost:8092/file/stay/9/20220330-093656-9cat.jpg,http://localhost:8092/file/stay/9/20220330-093530-8cat.jpg'
 where id = 9;

update stays
set photo_urls = 'http://localhost:8092/file/stay/10/24Storage-The-Cat-Flat-1.jpg,http://localhost:8092/file/stay/10/24Storage-The-Cat-Flat-5.jpg,' ||
                 'http://localhost:8092/file/stay/10/24Storage-The-Cat-Flat-7m.jpg'
where id = 10;

update stays
set photo_urls = 'http://localhost:8092/file/stay/23/booking_cat2.jpg,http://localhost:8092/file/stay/23/home-design.jpg'
where id = 23;

update stays
set photo_urls = 'http://localhost:8092/file/stay/1/57419-1.jpeg,http://localhost:8092/file/stay/1/57424-1.jpeg'
where id = 1;

update stays
set photo_urls = 'http://localhost:8092/file/stay/2/catville1.jpg,http://localhost:8092/file/stay/2/catville2.jpg'
where id = 2;

update stays
set photo_urls = 'http://localhost:8092/file/stay/3/Cat-Boarding.jpg,http://localhost:8092/file/stay/3/cat-boarding-1.jpg'
where id = 3;

update stays
set photo_urls = 'http://localhost:8092/file/stay/4/booking_cat1.jpg,http://localhost:8092/file/stay/4/sir_cat1.jpg,' ||
                 'http://localhost:8092/file/stay/4/sir_cat2.jpg'
where id = 4;

update stays
set photo_urls = 'http://localhost:8092/file/stay/5/h-1.jpg,http://localhost:8092/file/stay/5/h-2.jpg'

where id = 5;

update stays
set photo_urls = 'http://localhost:8092/file/stay/6/b-1.jpg,http://localhost:8092/file/stay/6/b-2.jpg'
where id = 6;

update stays
set photo_urls = 'http://localhost:8092/file/stay/7/c-2.jpg,http://localhost:8092/file/stay/7/c-1.jpg'
where id = 7;

update stays
set photo_urls = 'http://localhost:8092/file/stay/8/e-1.jpg,http://localhost:8092/file/stay/8/e-2.jpg,' ||
                 'http://localhost:8092/file/stay/8/e-3.jpeg'
where id = 8;

update stays
set photo_urls = 'http://localhost:8092/file/stay/11/modern-cat-furniture-indot-1.jpg,http://localhost:8092/file/stay/11/k-1.jpg'
where id = 11;

update stays
    set description =
                        'Мы на рынке уже 7 лет, и первые, кто объединил круглосуточное видеонаблюдение и отель для животных. ' ||
                        'Команда экспертов организовала идеальные условия содержания кошек. Каждому клиенту выдаем инструкцию: ' ||
                        'что взять с собой, как разместить животное и подключиться к видеонаблюдению. ' ||
                        'За постояльцами ухаживают 10 заботливых кото-нянь: ежедневно прибираются, кормят по индивидуальному ' ||
                        'расписанию, играют и гладят.'
where id in (1, 2, 3, 4);

update stays
set description =
                    'Наша гостиница для домашних животных принимает к проживанию только представителей ' ||
                    'семейства кошачьих. Они наши любимчики и лучшие друзья! Каждому постояльцу отдаем ' ||
                    'всю свою любовь и заботу. Он точно не почувствует себя одиноким. А даже наоборот, ' ||
                    'подумает, что это он в отпуске, а не Вы. ВАЖНО: в отель принимаются только привитые ' ||
                    'коты и кошки. Взрослые коты должны быть кастрированы.'
where id in (6, 7);

update stays
set description =
                        'В отеле для животных четвероногие находятся под пристальным наблюдением опытных ' ||
                        'котонянь. Они кормят котиков в соответствии с привычным режимом питания. Вовремя ' ||
                        'чистят лоток, чтобы им было максимально комфортно. Обязательно весело играют с ' ||
                        'ними и гладят по спинке.'
where id = 5;

update stays
set description =
                    'Наш отель для кошек – уютное место, где вы можете спокойно оставить любимца на передержку. ' ||
                    'Неважно, отправляетесь вы в отпуск, командировку или делаете ремонт. Двери нашей гостиницы ' ||
                    'всегда открыты для домашних животных.'
where id = 8;

update stays
set description =
                    'Уютная нано-гостиница для кошек, где каждый пушистый постоялец окружен заботой и ' ||
                    'вниманием, получая ненавязчивый уход как дома. Обратившись к нам, вы можете быть уверены, ' ||
                    'что отдых любимого питомца пройдет в лучших условиях. Передержка осуществляется в ' ||
                    'подготовленном для пребывания кошек доме, кварцуемом регулярно.'
where id in (9, 10, 11, 23);

insert into bookings (id, cat_amount, date_begin, date_end, total_cost, guest_id, stay_id)
values 
(nextval('bookings_seq'), 4, '2023-06-09', '2023-06-26', 9309.88, 2, 7),
(nextval('bookings_seq'), 5, '2023-03-21', '2023-04-01', 7886.94, 3, 1),
(nextval('bookings_seq'), 2, '2023-03-14', '2023-04-12', 16420.65, 11, 2),
(nextval('bookings_seq'), 2, '2023-04-05', '2023-04-07', 19030.47, 13, 3),
(nextval('bookings_seq'), 5, '2023-03-29', '2023-03-31', 16358.91, 8, 5),
(nextval('bookings_seq'), 2, '2023-04-02', '2023-06-10', 14156.37, 8, 9), -- 02.04-16.05, 29.05-10.06 1 место
(nextval('bookings_seq'), 2, '2023-06-07', '2023-06-14', 8498.23, 11, 7),
(nextval('bookings_seq'), 1, '2023-05-17', '2023-05-28', 17172.07, 13, 9), -- 17-28.05 0 мест
(nextval('bookings_seq'), 2, '2023-04-23', '2023-05-07', 19838.97, 3, 6),
(nextval('bookings_seq'), 5, '2023-05-01', '2023-05-25', 18642.73, 2, 4),
(nextval('bookings_seq'), 2, '2023-05-25', '2023-06-01', 17591.42, 10, 8),
(nextval('bookings_seq'), 4, '2023-06-15', '2023-06-13', 5442.49, 10, 3),
(nextval('bookings_seq'), 1, '2023-05-21', '2023-05-27', 6054.84, 15, 5),
(nextval('bookings_seq'), 3, '2023-04-25', '2023-04-28', 5758.94, 3, 10), -- 25-28.04 0 мест
(nextval('bookings_seq'), 1, '2023-05-11', '2023-05-13', 17395.53, 15, 6);

insert into prices
values
        (nextval('prices_seq'), '2023-04-01', 500, '2023-04-30', 11),
        (nextval('prices_seq'), '2023-05-01', 600, '2023-05-13', 11),
        (nextval('prices_seq'), null, 550, null, 11),
        (nextval('prices_seq'), '2023-04-01', 1000, '2023-04-30', 1),
        (nextval('prices_seq'), '2023-05-01', 1500, '2023-05-13', 1),
        (nextval('prices_seq'), null, 1200, null, 1),
        (nextval('prices_seq'), '2023-05-01', 1550, '2023-05-10', 2),
        (nextval('prices_seq'), null, 1250, null, 2),
        (nextval('prices_seq'), '2023-04-01', 1100, '2023-04-30', 3),
        (nextval('prices_seq'), '2023-05-01', 1400, '2023-05-13', 3),
        (nextval('prices_seq'), null, 1200, null, 3),
        (nextval('prices_seq'), '2023-04-14', 1550, '2023-04-28', 4),
        (nextval('prices_seq'), null, 1150, null, 4),
        (nextval('prices_seq'), '2023-04-14', 1500, '2023-05-28', 5),
        (nextval('prices_seq'), null, 1150, null, 5),
        (nextval('prices_seq'), '2023-04-14', 1550, '2023-05-10', 6),
        (nextval('prices_seq'), null, 1050, null, 6),
        (nextval('prices_seq'), '2023-04-24', 1300, '2023-05-12', 7),
        (nextval('prices_seq'), null, 1000, null, 7),
        (nextval('prices_seq'), '2023-04-14', 1500, '2023-05-11', 8),
        (nextval('prices_seq'), null, 1150, null, 8),
        (nextval('prices_seq'), '2023-04-28', 1000, '2023-05-12', 9),
        (nextval('prices_seq'), null, 850, null, 9),
        (nextval('prices_seq'), '2023-04-27', 1150, '2023-05-11', 10),
        (nextval('prices_seq'), null, 800, null, 10);



--эта таблица добавляет последнее число включительно, так что идет переплата за один день?
--без учета количетсва свободных мест

-- create temp table tbl ON COMMIT DROP as
create temp table IF NOT EXISTS dates as
SELECT
    COALESCE((:dateBegin + (Begin.X + 1) * interval '1day'), :dateBegin) AS date
FROM
    (SELECT
             B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
     FROM
         (SELECT 0 AS X UNION SELECT 1) AS B0,
         (SELECT 0 AS X UNION SELECT 1) AS B1,
         (SELECT 0 AS X UNION SELECT 1) AS B2,
         (SELECT 0 AS X UNION SELECT 1) AS B3,
         (SELECT 0 AS X UNION SELECT 1) AS B4,
         (SELECT 0 AS X UNION SELECT 1) AS B5
     WHERE
                 B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < DATE_PART('day', :date_end - :date_begin)) AS Begin
        FULL JOIN (SELECT
    B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
FROM
    (SELECT 0 AS X UNION SELECT 1) AS B0,
    (SELECT 0 AS X UNION SELECT 1) AS B1,
    (SELECT 0 AS X UNION SELECT 1) AS B2,
    (SELECT 0 AS X UNION SELECT 1) AS B3,
    (SELECT 0 AS X UNION SELECT 1) AS B4,
    (SELECT 0 AS X UNION SELECT 1) AS B5
WHERE
    B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < DATE_PART('day', :dateEnd - :dateBegin)) AS Ending
        ON (Begin.X + 1 = Ending.X)
ORDER BY
    date;

create temp table IF NOT EXISTS default_prices as
select stay_id, price from prices
where from_date is null and to_date is null;


select s.id, :catAmount * sum(COALESCE(p.price, dp.price))
from stays s left join dates d on true left join prices p
                                                 on s.id = p.stay_id and d.date BETWEEN p.from_date AND p.to_date
             left join default_prices dp
                       on s.id = dp.stay_id
WHERE s.id NOT IN (SELECT s.id FROM bookings b left JOIN stays s on b.stay_id = s.id
                   WHERE (:dateBegin BETWEEN b.date_begin AND b.date_end)
                      OR (:dateEnd BETWEEN b.date_begin AND b.date_end)
                       AND b.is_canceled = false)
  AND s.spot_amount >= :catAmount
  AND s.city = :city
group by s.id;


--один большой запрос с проверкой количества занятых мест
SELECT s.id, :catAmount * sum(COALESCE(p.price, dp.price))
FROM stays s LEFT JOIN (SELECT
                            COALESCE((TO_DATE(:dateBegin, 'YYYY-MM-DD') + (Begin.X + 1)), TO_DATE(:dateBegin, 'YYYY-MM-DD')) AS date
                        FROM
                            (SELECT
                                     B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
                             FROM
                                 (SELECT 0 AS X UNION SELECT 1) AS B0,
                                 (SELECT 0 AS X UNION SELECT 1) AS B1,
                                 (SELECT 0 AS X UNION SELECT 1) AS B2,
                                 (SELECT 0 AS X UNION SELECT 1) AS B3,
                                 (SELECT 0 AS X UNION SELECT 1) AS B4,
                                 (SELECT 0 AS X UNION SELECT 1) AS B5
                             WHERE
                                         B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < TO_DATE(:dateEnd, 'YYYY-MM-DD') - TO_DATE(:dateBegin, 'YYYY-MM-DD')) AS Begin
                                FULL JOIN (SELECT
                                                   B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
                                           FROM
                                               (SELECT 0 AS X UNION SELECT 1) AS B0,
                                               (SELECT 0 AS X UNION SELECT 1) AS B1,
                                               (SELECT 0 AS X UNION SELECT 1) AS B2,
                                               (SELECT 0 AS X UNION SELECT 1) AS B3,
                                               (SELECT 0 AS X UNION SELECT 1) AS B4,
                                               (SELECT 0 AS X UNION SELECT 1) AS B5
                                           WHERE
                                                       B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < TO_DATE(:dateEnd, 'YYYY-MM-DD') - TO_DATE(:dateBegin, 'YYYY-MM-DD')) AS Ending
                                          ON (Begin.X + 1 = Ending.X)
                        ORDER BY date) AS d ON TRUE

             LEFT JOIN prices p
                    ON s.id = p.stay_id and d.date BETWEEN p.from_date AND p.to_date
             LEFT JOIN (SELECT stay_id, price FROM prices
                        WHERE from_date IS NULL AND to_date IS NULL) AS dp
                    ON s.id = dp.stay_id
LEFT JOIN (SELECT b.stay_id, sum(b.cat_amount) as occupied_spots
             FROM bookings b
            WHERE (:dateBegin BETWEEN b.date_begin AND b.date_end)
                    OR (:dateEnd BETWEEN b.date_begin AND b.date_end)
                    OR :dateBegin < b.date_begin AND :dateEnd > b.date_end
              AND b.is_canceled = false
            GROUP BY b.stay_id) AS unavailable_stays
          ON s.id = unavailable_stays.stay_id
WHERE s.city = :city
  AND s.spot_amount >= COALESCE(unavailable_stays.occupied_spots, 0) + :catAmount
GROUP BY s.id;


--эта таблица добавляет последнее число включительно, так что идет переплата за один день

-- create temp table tbl ON COMMIT DROP as
create temp table IF NOT EXISTS dates as
SELECT
    COALESCE(('2023-01-14'::timestamp + (Begin.X + 1) * interval '1day'), '2023-01-14'::timestamp) AS date
FROM
    (SELECT
             B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
     FROM
         (SELECT 0 AS X UNION SELECT 1) AS B0,
         (SELECT 0 AS X UNION SELECT 1) AS B1,
         (SELECT 0 AS X UNION SELECT 1) AS B2,
         (SELECT 0 AS X UNION SELECT 1) AS B3,
         (SELECT 0 AS X UNION SELECT 1) AS B4,
         (SELECT 0 AS X UNION SELECT 1) AS B5
     WHERE
                 B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < DATE_PART('day', '2023-02-24'::timestamp - '2023-01-14'::timestamp)) AS Begin
        FULL JOIN (SELECT
                           B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
                   FROM
                       (SELECT 0 AS X UNION SELECT 1) AS B0,
                       (SELECT 0 AS X UNION SELECT 1) AS B1,
                       (SELECT 0 AS X UNION SELECT 1) AS B2,
                       (SELECT 0 AS X UNION SELECT 1) AS B3,
                       (SELECT 0 AS X UNION SELECT 1) AS B4,
                       (SELECT 0 AS X UNION SELECT 1) AS B5
                   WHERE
                        B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < DATE_PART('day', '2023-02-24'::timestamp - '2023-01-14'::timestamp)) AS Ending
ON (Begin.X + 1 = Ending.X)
ORDER BY
    date;


create temp table IF NOT EXISTS default_prices as
select stay_id, price from prices
where from_date is null and to_date is null;


select s.*, 3 * sum(COALESCE(p.price, dp.price))
    from stays s left join dates d on true left join prices p
    on s.id = p.stay_id and d.date BETWEEN p.from_date AND p.to_date
                 left join default_prices dp
    on s.id = dp.stay_id
WHERE s.id NOT IN (SELECT s.id FROM bookings b left JOIN stays s on b.stay_id = s.id
                   WHERE ('2023-01-14'::timestamp BETWEEN b.date_begin AND b.date_end)
                      OR ('2023-02-24'::timestamp BETWEEN b.date_begin AND b.date_end)
                      OR '2023-01-14'::timestamp < b.date_begin AND '2023-02-24'::timestamp > b.date_end
                       AND b.is_canceled = false)
  AND s.spot_amount >= 3
  AND s.city = 'Москва'
group by s.id;




select dates.date, p.price, p.stay_id from prices p right join dates
    on (date BETWEEN p.from_date AND p.to_date)
        left join stays on p.price is null;

drop table dates;




--один большой запрос с проверкой количества свободных мест
select s.*, 3 * sum(COALESCE(p.price, dp.price))
from stays s left join (SELECT
                            COALESCE(('2023-03-21'::timestamp + (Begin.X + 1) * interval '1day'), '2023-03-21'::timestamp) AS date
                        FROM
                            (SELECT
                                     B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
                             FROM
                                 (SELECT 0 AS X UNION SELECT 1) AS B0,
                                 (SELECT 0 AS X UNION SELECT 1) AS B1,
                                 (SELECT 0 AS X UNION SELECT 1) AS B2,
                                 (SELECT 0 AS X UNION SELECT 1) AS B3,
                                 (SELECT 0 AS X UNION SELECT 1) AS B4,
                                 (SELECT 0 AS X UNION SELECT 1) AS B5
                             WHERE
                                         B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < DATE_PART('day', '2023-04-09'::timestamp - '2023-03-21'::timestamp)) AS Begin
                                FULL JOIN (SELECT
                                                   B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) AS X
                                           FROM
                                               (SELECT 0 AS X UNION SELECT 1) AS B0,
                                               (SELECT 0 AS X UNION SELECT 1) AS B1,
                                               (SELECT 0 AS X UNION SELECT 1) AS B2,
                                               (SELECT 0 AS X UNION SELECT 1) AS B3,
                                               (SELECT 0 AS X UNION SELECT 1) AS B4,
                                               (SELECT 0 AS X UNION SELECT 1) AS B5
                                           WHERE
                                                       B0.X + 2 * (B1.X + 2 * (B2.X + 2 * (B3.X + 2 * (B4.X + 2 * B5.X)))) < DATE_PART('day', '2023-04-09'::timestamp - '2023-03-21'::timestamp)) AS Ending
                                          ON (Begin.X + 1 = Ending.X)
                        ORDER BY date) AS d on true

             left join prices p
                    on s.id = p.stay_id and d.date BETWEEN p.from_date AND p.to_date
             left join (select stay_id, price from prices
                        where from_date is null and to_date is null) as dp
                       on s.id = dp.stay_id
LEFT JOIN (SELECT b.stay_id, sum(b.cat_amount) as occupied_spots FROM bookings b
                   WHERE ('2023-03-21'::timestamp BETWEEN b.date_begin AND b.date_end)
                      OR ('2023-04-09'::timestamp BETWEEN b.date_begin AND b.date_end)
                      OR '2023-03-21'::timestamp < b.date_begin AND '2023-04-09'::timestamp > b.date_end
                       AND b.is_canceled = false
                       GROUP BY b.stay_id) AS unavailable_stays
          ON s.id = unavailable_stays.stay_id
where s.city = 'Москва'
  AND s.spot_amount >= COALESCE(unavailable_stays.occupied_spots, 0) + 15
group by s.id;

--проверка на кол-во занятых мест
select s.id, s.stay_name, s.city, s.spot_amount, unavailable_stays.occupied_spots
from stays s left join (SELECT b.stay_id, sum(b.cat_amount) as occupied_spots FROM bookings b
                        WHERE ('2023-03-21'::timestamp BETWEEN b.date_begin AND b.date_end)
                           OR ('2023-04-09'::timestamp BETWEEN b.date_begin AND b.date_end)
                           OR '2023-03-21'::timestamp < b.date_begin AND '2023-04-09'::timestamp > b.date_end
                            AND b.is_canceled = false
                        GROUP BY b.stay_id) AS unavailable_stays
ON s.id = unavailable_stays.stay_id
  where s.city = 'Москва'
    AND s.spot_amount >= COALESCE(unavailable_stays.occupied_spots, 0) + 15;


SELECT b.stay_id, sum(b.cat_amount) as occupied_spots FROM bookings b
WHERE ('2023-03-21'::timestamp BETWEEN b.date_begin AND b.date_end)
   OR ('2023-04-09'::timestamp BETWEEN b.date_begin AND b.date_end)
   OR '2023-03-21'::timestamp < b.date_begin AND '2023-04-09'::timestamp > b.date_end
    AND b.is_canceled = false
GROUP BY b.stay_id;

SELECT s.id, s.stay_name, s.city, s.address, s.spot_amount, SUM(COALESCE(b.cat_amount, 0)) as occupied_spots
FROM stays s LEFT JOIN bookings b ON s.id = b.stay_id
WHERE s.is_deleted = false
  AND s.owner_id = :ownerId
  AND (now() BETWEEN b.date_begin AND b.date_end or b.id is null)
GROUP BY s.id;




