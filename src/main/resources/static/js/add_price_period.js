function addPricePeriod() {

    const tableBody = document.getElementById('tableBody');
    const row = tableBody.insertRow(0);
    const cell1 = row.insertCell(0);
    const cell2 = row.insertCell(1);
    const cell3 = row.insertCell(2);
    const cell4 = row.insertCell(3);
    cell1.innerHTML = '<td><input class="form-control" name="fromDate" type="date" required/></td>';
    cell2.innerHTML = '<td><input class="form-control" name="toDate" type="date" required/></td>';
    cell3.innerHTML = '<td><input class="form-control" name="price" type="number" step="0.01" min="1" required/></td>';
    cell4.innerHTML = '<td><a href="">Удалить</a>' +
        '<a href="#" onclick="submit()" type="submit"' +
        '                    style="float: right">Сохранить</button>' +
        '</td>';

}

function submit() {
    validateForm();
    document.getElementById("newPriceForm").submit();
}