$("#address").suggestions({
    token: "ca4e9687e92c8dae968810c84bacd8eceb752e57",
    type: "ADDRESS",
    /* Вызывается, когда пользователь выбирает одну из подсказок */
    onSelect: function(suggestion) {
        console.log(suggestion);
        const info = jQuery.parseJSON(JSON.stringify(suggestion));
        $("#city").val(info.data.city);
        $("#longitude").val(info.data.geo_lon);
        $("#latitude").val(info.data.geo_lat);
    }
});