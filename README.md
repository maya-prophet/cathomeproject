# Online Pet Hotel Aggregator

Service that allows pet hotel owners and small at-home pet boarding owners to place their stays information on the web-site, receive and handle bookings, manage prices (default and special period ones), reach new clients.

For pet owners it offeres easy access to aggregated stay information in one place, ability to search for an appropriate stay by a set of parameters, compare prices and services, book and manage their bookings online.


# Purpose of the application

Simplify communication between stay owners and pet owners, make it easier to access otherwise scattered over the internet information, increase competitiveness in the field, and consequently improve the quality of services and reduce prices.

## Screenshots

![project_screenshots_gif](/source/images/project_screenshots.gif)
Template designed by
<a href="https://htmlcodex.com">HTML Codex</a>

## MVP functionality
Stay owner and cat owner profile pages are available.

Stay owner can:
 - put in information about stays 
 - manage stays and their prices 
 - manage bookings

Cat owner can:
 - search for a suitable stay by parameters (dates, city, cat number)
 - book a stay
 - manage bookings

## Future plans
 - Admin panel
 - Stay reviews and ratings
 - Direct communication between stay owner and cat owner
